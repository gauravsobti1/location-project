package adapter;

import java.util.ArrayList;

import com.android.volley.toolbox.ImageLoader;


import com.android.volley.toolbox.NetworkImageView;
import com.example.location.update.AppController;
import com.example.locationn.R;

import adapter_data.ParticularTypePlaceData;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

public class ParticularTypePlaceListViewAdapter extends ArrayAdapter<ParticularTypePlaceData> implements Filterable {
	private LayoutInflater mInflater;
	private ArrayList<ParticularTypePlaceData> objects;
	private ArrayList<ParticularTypePlaceData> filteredObjects;
	private Filter filter;
	ImageLoader imageLoader=AppController.getInstance().getImageLoader();
	private int resource;



	public ParticularTypePlaceListViewAdapter(Context context, int resource,
			ArrayList<ParticularTypePlaceData> objects) {
		super(context, resource, objects);
		this.objects = objects;
		filteredObjects = objects;
		this.resource = resource;
	}
	
	
	@Override
	public ParticularTypePlaceData getItem(int position) {
		// TODO Auto-generated method stub
		return filteredObjects.get(position);
	}
	
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return filteredObjects.size();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Holder holder;
		Log.e("Inside getView","Inside getView");
		
		if(mInflater==null)
			mInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		if (convertView == null) {
	            // Inflate the view since it does not exist
	            convertView = mInflater.inflate(resource, null);

	            // Create and save off the holder in the tag so we get quick access to inner fields
	            // This must be done for performance reasons
	            holder = new Holder();
	            holder.image = (NetworkImageView) convertView.findViewById(R.id.Image);
	            holder.placeName = (TextView) convertView.findViewById(R.id.placeName);
	           /* holder.Website = (TextView) convertView.findViewById(R.id.brandsWebsite);
	            holder.BrandImage = (NetworkImageView) convertView.findViewById(R.id.brandImage);*/
	            convertView.setTag(holder);
	            } else {
	            holder = (Holder) convertView.getTag();
	        }
		  if(imageLoader==null)
			  imageLoader = AppController.getInstance().getImageLoader();
		 
	       holder.image.setImageUrl(getItem(position).getImage_url(), imageLoader);
	  
				holder.placeName.setText(getItem(position).getPlace_name()/*+" - " + getItem(position).getAddress()*/);
			
		
		return convertView;
	}
	
	
	
	
	@Override
	public Filter getFilter() {
		if(filter == null)
            filter = new MyFilter();
        return filter;
	}
	
	private static class Holder
	{
		
		NetworkImageView image;
		
		TextView placeName;

	}
	
	class MyFilter extends Filter
	{

		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			String constraints = constraint.toString().toLowerCase();
			  
            FilterResults result = new FilterResults();
            if(constraints!= null && constraints.length() > 0 ) 
            {
                ArrayList<ParticularTypePlaceData> filt = new ArrayList<ParticularTypePlaceData>();
                ArrayList<ParticularTypePlaceData> lItems = new ArrayList<ParticularTypePlaceData>();
                synchronized (this)
                {
                    lItems.addAll(objects);
                }
                for(int i = 0, l = lItems.size(); i < l; i++)
                {
                    ParticularTypePlaceData m = lItems.get(i);
                    if((m.getPlace_name().toLowerCase()).contains(constraints)  )
                        filt.add(m);
                }
                result.count = filt.size();
                result.values = filt;
            }
            else
            {
                synchronized(this)
                {
                    result.values = objects;
                    result.count = objects.size();
                }
            }
            return result;

		}

		@Override
		protected void publishResults(CharSequence constraint,
				FilterResults results) {
			filteredObjects = (ArrayList<ParticularTypePlaceData>)results.values;
            notifyDataSetChanged();
			
		}
		
	}
	
}
