package com.example.location.update;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.example.locationn.DealAlarmReceiver;

import android.app.Application;
import android.app.PendingIntent;
import android.content.Intent;
import android.text.TextUtils;

public class AppController extends Application {

	public static final String TAG = AppController.class.getSimpleName();
	 
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    private PendingIntent pendingIntent;
    private static AppController mInstance;
    
    @Override
    public void onCreate() {
    	// TODO Auto-generated method stub
    	super.onCreate();
    	mInstance = this;
    }
    
    
    public static synchronized AppController getInstance() {
        return mInstance;
    }
    
    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
 
        return mRequestQueue;
    }
 
    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }
    
    public PendingIntent getPendingIntent()
    {
    	if (pendingIntent == null)
    	{
    		pendingIntent = PendingIntent.getBroadcast(this, 0, new Intent(this, DealAlarmReceiver.class), 0);
    	}
    	
    	return pendingIntent;
    }
    
    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }
 
    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }
 
    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
    
    
    
    
}
