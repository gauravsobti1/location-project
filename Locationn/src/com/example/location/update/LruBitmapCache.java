package com.example.location.update;

import com.android.volley.toolbox.ImageLoader.ImageCache;

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

public class LruBitmapCache extends LruCache<String, Bitmap> implements ImageCache{

	public static int getDefaultLruCacheSize() {
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        final int cacheSize = maxMemory / 8;
 
        return cacheSize;
    }
	
	public LruBitmapCache() {
		this(getDefaultLruCacheSize());
		// TODO Auto-generated constructor stub
	}
	
	 public LruBitmapCache(int sizeInKiloBytes) {
	        super(sizeInKiloBytes);
	    }

	 @Override
	protected int sizeOf(String key, Bitmap value) {
		// TODO Auto-generated method stub
		/*return super.sizeOf(key, value);*/
		 return value.getRowBytes() * value.getHeight()/1024;
		 
	}
	 
	@Override
	public Bitmap getBitmap(String arg0) {
		// TODO Auto-generated method stub
		return get(arg0);
	}

	@Override
	public void putBitmap(String arg0, Bitmap arg1) {
		put(arg0,arg1);
		
	}

}
