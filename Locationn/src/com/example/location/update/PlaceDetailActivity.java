package com.example.location.update;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.locationn.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import adapter_data.ParticularTypePlaceData;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.widget.TextView;

public class PlaceDetailActivity extends ActionBarActivity implements OnMapReadyCallback{
	
	private double latitude;
	private double longitude;
	private SupportMapFragment mapFragment;
	private CameraPosition storePosition;
	private TextView place_details;
	private String name;
	private ActionBar actionbar;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.places_detail_activity);
		 actionbar = getSupportActionBar();
		
		/*actionbar.setBackgroundDrawable(newcolor);*/
		actionbar.setDisplayHomeAsUpEnabled(true);
	//	actionbar.setDisplayShowHomeEnabled(false);
		actionbar.setTitle(getIntent().getStringExtra("name"));
		
		place_details = (TextView) findViewById(R.id.place_detail);
		
		loadData();
		
		mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
		
		
	}
	
	
	private void setUpViews()
	{
		
		setUpCameraPosition();
		mapFragment.getMapAsync(this);
	}
	
	
	
	private void loadData()
	{
		String particularTypePlaceUrl = "https://maps.googleapis.com/maps/api/place/details/json?placeid=";
		StringBuilder sb = new StringBuilder();
		sb.append(particularTypePlaceUrl);
		sb.append(getIntent().getStringExtra("place_id"));
		
		sb.append("&key=AIzaSyDlBsviEYBk93na8VoL-lWBax2L8orGrV4");
		
		JsonObjectRequest jsonRequest = new JsonObjectRequest(sb.toString(), null, new Response.Listener<JSONObject>() {

			

			

			@Override
			public void onResponse(JSONObject arg0) {
				//Log.e("Response = ", arg0.toString());
				try {
					JSONObject results = arg0.getJSONObject("result");
					 name = results.getString("name");
					//JSONObject dealsObject=null;
					StringBuilder sb1 = new StringBuilder();
					sb1.append("Name = ");
					sb1.append(name);
					sb1.append("\nAddress = ");
					sb1.append(results.getString("formatted_address"));
					/*sb1.append("\nPhone Number = ");
					sb1.append(results.getString("formatted_phone_number"));*/
					place_details.setText(sb1.toString());
					
					
					JSONObject location = results.getJSONObject("geometry").getJSONObject("location");
					latitude=Double.parseDouble(location.getString("lat"));
					longitude = Double.parseDouble(location.getString("lng"));
					
					setUpViews();
					actionbar.setTitle(name);
					
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					//Toast.makeText(getActivity().getApplicationContext(), "No More Deals Available", Toast.LENGTH_LONG).show();
					
				}
				
				
				
				
			}
		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				
				
			}
		}
		
	);

			
		
		
		 AppController.getInstance().addToRequestQueue(jsonRequest);
	}
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		 switch (item.getItemId()) {
	     case android.R.id.home:
	         
	         finish();

	         return true;
	         
		default:
		return super.onOptionsItemSelected(item);
		 }
	}


	@Override
	public void onMapReady(GoogleMap map) {
		// TODO Auto-generated method stubmap.addMarker(new MarkerOptions().position(new LatLng(lat, longitude)).title(brand_name));
		
		
		map.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).title(name));
		map.animateCamera(CameraUpdateFactory.newCameraPosition(storePosition), 2, null);
		
		
	}
	
	private void setUpCameraPosition() {
		storePosition= new CameraPosition.Builder().target(new LatLng(latitude, longitude))
         .zoom(15.5f)
         .bearing(300)
         .tilt(25)
         .build();
		
	}
	
	
	

}
