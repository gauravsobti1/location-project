package com.example.location.update;


import com.example.locationn.R;

import android.app.AlarmManager;
import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceActivity;
import android.preference.RingtonePreference;
import android.util.Log;
import android.view.MenuItem;
import android.view.Window;

public class SettingsActivity extends PreferenceActivity{
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		//requestWindowFeature(Window.FEATURE_ACTION_BAR);
		super.onCreate(savedInstanceState);
		//getActionBar().setDisplayHomeAsUpEnabled(true);
		addPreferencesFromResource(R.xml.settings_main);
		
		RingtonePreference ringtone_preference = (RingtonePreference)getPreferenceScreen().getPreference(1);
		
		ringtone_preference.setDefaultValue((PreferenceManager.
		         getDefaultSharedPreferences(getBaseContext())).getString("ringtone", ""));
		ringtone_preference.setShowDefault(true);
		ringtone_preference.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
			
			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				
				Log.v("HereApp", "Preference Changed "+ " New Value = " + newValue.toString());
				
				return false;
			}
		});
		
		 final CheckBoxPreference loc_update_preference = (CheckBoxPreference)getPreferenceScreen().getPreference(0);
		
		try {
			Boolean value = (PreferenceManager.
			         getDefaultSharedPreferences(getBaseContext())).getBoolean("pref_stop_updates", false) ;
			Log.i ("HereApp", "Boolean Value = "+ value); 
			loc_update_preference.setDefaultValue(value);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
				loc_update_preference.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
			
			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				AlarmManager alarmMgr = (AlarmManager) SettingsActivity.this.getSystemService(Context.ALARM_SERVICE);
				if(newValue.toString().equalsIgnoreCase("false"))
				{
					loc_update_preference.setChecked(false);
					 alarmMgr.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + 10000,
				              60* 1000, AppController.getInstance().getPendingIntent());
				}
				
				else
				{
					loc_update_preference.setChecked(true);
					
					alarmMgr.cancel(AppController.getInstance().getPendingIntent());
					
				}
				
				
				Log.v("HereApp", "Location Update Preference Changed "+ " New Value = " + newValue.toString());
				
				return false;
			}
		});
	
	}
	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		 switch (item.getItemId()) {
	     case android.R.id.home:
	         
	         finish();

	         return true;
	         
		default:
		return super.onOptionsItemSelected(item);
		 }
	
	}
	
	
	
}
