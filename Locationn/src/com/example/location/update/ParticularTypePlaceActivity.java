package com.example.location.update;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.locationn.R;

import adapter.ParticularTypePlaceListViewAdapter;
import adapter_data.ParticularTypePlaceData;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnCloseListener;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.support.v7.widget.SearchView.SearchAutoComplete;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ParticularTypePlaceActivity extends ActionBarActivity{

	private String type;

	private ArrayList<ParticularTypePlaceData> listdata = new ArrayList<ParticularTypePlaceData>();

	private ParticularTypePlaceListViewAdapter adapter;

	private SearchView searchview;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.types_of_places_frag);
		ActionBar actionbar = getSupportActionBar();
		actionbar.setDisplayShowCustomEnabled(true);
				actionbar.setDisplayHomeAsUpEnabled(true);
				actionbar.setCustomView(R.layout.ab_custom_view);
				View v = actionbar.getCustomView();
				final TextView title = (TextView) v.findViewById(R.id.titleText);
				
				
				searchview=(SearchView)v.findViewById(R.id.searchView);
			
				searchview.setOnCloseListener(new OnCloseListener() {
					
					@Override
					public boolean onClose() {
						if(title.getVisibility()== View.GONE)
							title.setVisibility(View.VISIBLE);
						return false;
					}
				});
				
				searchview.setOnSearchClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						if(title.getVisibility()== View.VISIBLE)
						title.setVisibility(View.GONE);
					}
				});
				
				
				//adapter = new TypesOfPlacesListViewAdapter(getActivity(), android.R.layout.simple_list_item_1,				typesOfPlaces);
					
				SearchAutoComplete autoComplete = (SearchView.SearchAutoComplete)searchview.findViewById(R.id.search_src_text);
				/*autoComplete.setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, searchViewAdapter));*/
				
				
				autoComplete.setTextColor(Color.WHITE);
				
				searchview.setOnQueryTextListener(new OnQueryTextListener() {
					
					@Override
					public boolean onQueryTextSubmit(String arg0) {
						// TODO Auto-generated method stub
						return false;
					}
					
					@Override
					public boolean onQueryTextChange(String arg0) {
						adapter.getFilter().filter(arg0);
						return false;
					}
				});
				
		ListView listview = (ListView) findViewById(R.id.types_of_nearby_places);
		
		type=getIntent().getStringExtra("type");
		title.setText(type);
		adapter = new ParticularTypePlaceListViewAdapter(this, R.layout.particular_type_place_listview, listdata);
		listview.setAdapter(adapter);
		loaddata();
		
		listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				
				Intent intent = new Intent(ParticularTypePlaceActivity.this, PlaceDetailActivity.class);
				intent.putExtra("place_id", listdata.get(arg2).getPlace_id());
				startActivity(intent);
				
				
			}
		});
		
	}
	
	private void loaddata()
	{
		
		//https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=28.0531,77.0847589&
		//radius=100&types=bank&key=AIzaSyCTPlCohjsnYAZPfCJIb4nmtuJXaVBf0rU
			
		String particularTypePlaceUrl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=";
		StringBuilder sb = new StringBuilder();
		sb.append(particularTypePlaceUrl);
		sb.append("28.6470,77.0933&rankby=distance&types=");
		sb.append(type);
		sb.append("&key=AIzaSyDlBsviEYBk93na8VoL-lWBax2L8orGrV4");
		
		JsonObjectRequest jsonRequest = new JsonObjectRequest(sb.toString(), null, new Response.Listener<JSONObject>() {

			

			@Override
			public void onResponse(JSONObject arg0) {
				//Log.e("Response = ", arg0.toString());
				try {
					JSONArray resultsArray = arg0.getJSONArray("results");
									
					//JSONObject dealsObject=null;
					ParticularTypePlaceData Data =null;
					
					 
					for(int i=0; i<resultsArray.length();i++)
					{
						Data = new ParticularTypePlaceData();
						JSONObject object = resultsArray.getJSONObject(i);
						Data.setPlace_name(object.getString("name")); 
						Data.setPlace_id(object.getString("place_id")); 
						Data.setImage_url(object.getString("icon"));
						Data.setAddress(object.getString("vicinity"));
						listdata.add(Data);
					}
					
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					//Toast.makeText(getActivity().getApplicationContext(), "No More Deals Available", Toast.LENGTH_LONG).show();
					
				}
				
				//Toast.makeText(getActivity(), arg0.toString(), Toast.LENGTH_LONG).show();
				adapter.notifyDataSetChanged();
				
				
			}
		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				
				
			}
		}
		
	);

			
		
		
		 AppController.getInstance().addToRequestQueue(jsonRequest);
	}
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		 switch (item.getItemId()) {
         case android.R.id.home:
            /* FragmentManager fm= getSupportFragmentManager();
             fm.popBackStack();*/
             finish();

             return true;
             
		default:
		return super.onOptionsItemSelected(item);
	}
	}
	
	
}
