package com.example.location.update;


import java.util.ArrayList;







import com.example.locationn.ProjectActivity;
import com.example.locationn.R;






import adapter.ParticularTypePlaceListViewAdapter;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnCloseListener;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.support.v7.widget.SearchView.SearchAutoComplete;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class TypesOfPlacesFrag extends Fragment{
	
	private View vv;
	private ArrayList<String> typesOfPlaces;
	private SearchView searchview;
	/*private TypesOfPlacesListViewAdapter adapter;*/
	private ArrayAdapter< String> adapter;
	private ListView typeOfPlacesListView;
	


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		 vv = inflater.inflate(R.layout.types_of_places_frag, container,false);
		 typesOfPlaces = new ArrayList<String>();
		 typesOfPlaces.add("accounting");
		 typesOfPlaces.add("airport");
		 typesOfPlaces.add("amusement_park");
		 typesOfPlaces.add("aquarium");
		 typesOfPlaces.add("art_gallery");
		 typesOfPlaces.add("atm");
		 typesOfPlaces.add("bakery");
		 typesOfPlaces.add("bank");
		 typesOfPlaces.add("bar");
		 typesOfPlaces.add("beauty_salon");
		 typesOfPlaces.add("bicycle_store");
		 typesOfPlaces.add("book_store");
		 typesOfPlaces.add("bowling_alley");
		 typesOfPlaces.add("bus_station");
		 typesOfPlaces.add("cafe");
		 typesOfPlaces.add("car_dealer");
		 typesOfPlaces.add("car_wash");
		 typesOfPlaces.add("casino");
		 typesOfPlaces.add("church");
		 typesOfPlaces.add("clothing_store");
		 typesOfPlaces.add("convenience_store");
		 typesOfPlaces.add("courthouse");
		 typesOfPlaces.add("dentist");
		 typesOfPlaces.add("department_store");
		 typesOfPlaces.add("doctor");
		 typesOfPlaces.add("electrician");
		 typesOfPlaces.add("finance");
		 typesOfPlaces.add("fire_station");
		 typesOfPlaces.add("florist");
		 typesOfPlaces.add("food");
		 typesOfPlaces.add("furniture_store");
		 typesOfPlaces.add("gas_station");
		 typesOfPlaces.add("general_contractor");
		 typesOfPlaces.add("grocery_or_supermarket");
		 typesOfPlaces.add("gym");
		 typesOfPlaces.add("hair_care");
		 typesOfPlaces.add("hardware_store");
		 typesOfPlaces.add("health");
		 typesOfPlaces.add("hindu_temple");
		 typesOfPlaces.add("hospital");
		 typesOfPlaces.add("insurance_agency");
		 typesOfPlaces.add("jewelry_store");
		 typesOfPlaces.add("laundry");
		 typesOfPlaces.add("lawyer");
		 typesOfPlaces.add("library");
		 typesOfPlaces.add("liquor_store");
		 typesOfPlaces.add("local_government_office");
		 typesOfPlaces.add("locksmith");
		 typesOfPlaces.add("meal_delivery");
		 typesOfPlaces.add("meal_takeaway");
		 typesOfPlaces.add("movie_rental");
		 typesOfPlaces.add("movie_theater");
		 typesOfPlaces.add("moving_company");
		 typesOfPlaces.add("museum");
		 typesOfPlaces.add("night_club");
		 typesOfPlaces.add("park");
		 typesOfPlaces.add("parking");
		 typesOfPlaces.add("plumber");
		 typesOfPlaces.add("police");
		 typesOfPlaces.add("post_office");
		 typesOfPlaces.add("restaurant");
		 typesOfPlaces.add("school");
		 typesOfPlaces.add("spa");
		 typesOfPlaces.add("train_station");
		 typesOfPlaces.add("university");
		 typesOfPlaces.add("zoo");
		 
		 
		 
		 typeOfPlacesListView = (ListView) vv.findViewById(R.id.types_of_nearby_places);
		 
		return vv;
		
		
		
	}
	
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		
		ActionBar actionbar =((ProjectActivity)getActivity()).getSupportActionBar();
		actionbar.setDisplayShowCustomEnabled(true);
		actionbar.setCustomView(R.layout.ab_custom_view);
		View v = actionbar.getCustomView();
		final TextView title = (TextView) v.findViewById(R.id.titleText);
		
		searchview=(SearchView)v.findViewById(R.id.searchView);
	
		searchview.setOnCloseListener(new OnCloseListener() {
			
			@Override
			public boolean onClose() {
				if(title.getVisibility()== View.GONE)
					title.setVisibility(View.VISIBLE);
				return false;
			}
		});
		
		searchview.setOnSearchClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(title.getVisibility()== View.VISIBLE)
				title.setVisibility(View.GONE);
			}
		});
		
		
		//adapter = new TypesOfPlacesListViewAdapter(getActivity(), android.R.layout.simple_list_item_1,				typesOfPlaces);
		adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, typesOfPlaces);
		typeOfPlacesListView.setAdapter(adapter);
		
		SearchAutoComplete autoComplete = (SearchView.SearchAutoComplete)searchview.findViewById(R.id.search_src_text);
		/*autoComplete.setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, searchViewAdapter));*/
		
		
		autoComplete.setTextColor(Color.WHITE);
		
		searchview.setOnQueryTextListener(new OnQueryTextListener() {
			
			@Override
			public boolean onQueryTextSubmit(String arg0) {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public boolean onQueryTextChange(String arg0) {
				adapter.getFilter().filter(arg0);
				return false;
			}
		});
		
		
		typeOfPlacesListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Intent intent = new Intent(getActivity(), ParticularTypePlaceActivity.class);
				intent.putExtra("type", typesOfPlaces.get(arg2));
				startActivity(intent);
				
				
			}
		});
		
	}
	
	
	
	
	

}
