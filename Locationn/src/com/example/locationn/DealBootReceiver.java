package com.example.locationn;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.provider.SyncStateContract.Constants;
import android.util.Log;

public class DealBootReceiver extends BroadcastReceiver {

	   @Override
	   public void onReceive(Context context, Intent intent) {
	      Log.i("1", "DealBootReceiver invoked, configuring AlarmManager");
	      AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
	      
	      PendingIntent pendingIntent =
	               PendingIntent.getBroadcast(context, 0, new Intent(context, DealAlarmReceiver.class), 0);

	      // use inexact repeating which is easier on battery (system can phase events and not wake at exact times)
	      alarmMgr.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + 30000,
	               AlarmManager.INTERVAL_FIFTEEN_MINUTES, pendingIntent);
	      /*alarmMgr.cancel(pendingIntent);*/
	      
	      SharedPreferences sharedPref = context.getSharedPreferences("My Preference", Context.MODE_PRIVATE);
	      
	      SharedPreferences.Editor editor = sharedPref.edit();
		  editor.putInt("alarm_started",1);
		  
		  
		  editor.commit();
	      
	   }
	}