package com.example.locationn;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.app.Activity;
import android.content.Intent;

public class MainActivity extends Activity {

	
	ProgressBar pgBa;
	Thread t;
	Handler h;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
//Service code here
	/*	Intent ser= new Intent();
		ser.setClass(MainActivity.this, MyService.class);
		startService(ser);*/
		
		
		pgBa = (ProgressBar) findViewById(R.id.progressBar1);
		
		new MyTask().execute();
		
	/*	h=new Handler()
		{
			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
				pgBa.setProgress(msg.arg1);
			}
			
		};
		
		
	  t = new Thread(new Runnable() {
			
			@Override
			public void run() {
				
				for(int i=0;i<100;i++)
					
            	{Message message = Message.obtain();
					message.arg1=i;
            		h.sendMessage(message);
            		try {
						t.sleep(100);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
            	}
            	
				Intent mainIntent = new Intent(MainActivity.this,First.class);
                startActivity(mainIntent);
                
                finish();
				
			}
		});
		
		t.start();
	*/
		
		/*new Handler().postDelayed(new Runnable() {
            public void run() {
            	Message message= new Message();
            	
            	for(int i=1;i<=100;i++)
            	{
            		pgBa.setProgress(i);
            	}
            	
            	Intent mainIntent = new Intent(MainActivity.this,First.class);
                startActivity(mainIntent);
                
                finish();
            }
		},1000);*/
		
		
		//splash activity code here
		/*int secondsDelayed = 1;
        new Handler().postDelayed(new Runnable() {
                public void run() {
                	
                	Intent mainIntent = new Intent(MainActivity.this,First.class);
                    startActivity(mainIntent);
                    
                    finish();
                }
        }, secondsDelayed * 1000);*/
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	
	class MyTask extends AsyncTask<Void, Integer, Void>
	{

		@Override
		protected void onPreExecute() {
			pgBa.setIndeterminate(false);
			pgBa.setEnabled(true);
			pgBa.setVisibility(View.VISIBLE);
			
		//	pgBa.setProgress(0);
			
			
		}
		
		
		@Override
		protected Void doInBackground(Void... params) {
			
			for(int i=0;i<100;i+=2)
			{
				publishProgress(i);
				
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
			
			return null;
		}
		
		
		
		@Override
		protected void onProgressUpdate(Integer... values) {
			
			pgBa.setProgress(values[0]);
			
		}
		
		
		
		@Override
		protected void onPostExecute(Void result) {
			
			Intent mainIntent = new Intent(MainActivity.this,First.class);
            startActivity(mainIntent);
            
            finish();
			
		}
		
	}
}
