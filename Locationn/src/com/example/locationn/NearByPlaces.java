package com.example.locationn;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class NearByPlaces extends Fragment {
	
	TextView displayAddress;
	
	private Button continueButton;
	/*Location loc;*/
	private LatLng locaa;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.nearbyplaces_fragment, container,false);
		continueButton = (Button) v.findViewById(R.id.continue1);
		return v;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		
		
		
		SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
		mapFragment.getMapAsync(new OnMapReadyCallback() {
			
			@Override
			public void onMapReady(final GoogleMap googleMap) {
				googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
				// googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
				// googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
				// googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
				// googleMap.setMapType(GoogleMap.MAP_TYPE_NONE);

				// Showing / hiding your current location
				googleMap.setMyLocationEnabled(true);

				// Enable / Disable zooming controls
				googleMap.getUiSettings().setZoomControlsEnabled(true);

				// Enable / Disable my location button
				googleMap.getUiSettings().setMyLocationButtonEnabled(false);

				// Enable / Disable Compass icon
				googleMap.getUiSettings().setCompassEnabled(true);

				// Enable / Disable Rotate gesture
				googleMap.getUiSettings().setRotateGesturesEnabled(true);

				// Enable / Disable zooming functionality
				googleMap.getUiSettings().setZoomGesturesEnabled(true);
				googleMap.getUiSettings().setMyLocationButtonEnabled(true);
				
				
				//LatLng latttt= new LatLng(googleMap.getMyLocation().getLatitude(), googleMap.getMyLocation().getLongitude());
				LatLng latttt = new LatLng(28.142, 77.133);
				MarkerOptions marker = new MarkerOptions().position(latttt).title("Sample Location");
					
				marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_copy));
				googleMap.addMarker(marker);
				CameraPosition cameraPosition = new CameraPosition.Builder()
				.target(latttt).zoom(17).build();

	googleMap.animateCamera(CameraUpdateFactory
				.newCameraPosition(cameraPosition));

	googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
		
		

		@Override
		public void onMapClick(LatLng point) {
			 MarkerOptions options = new MarkerOptions();
			 googleMap.clear();
			 locaa=null;
                // Setting the position of the marker
                options.position(point);
			
			 options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
			 googleMap.addMarker(options);
			 locaa=point;
		}
	});
			}
		});
		
continueButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(locaa==null)
				{
					Toast.makeText(getActivity(), "Please select a location ", Toast.LENGTH_LONG).show();
				}
				else
				{
					/*double lati= loc.getLatitude();
					double longi = loc.getLongitude();*/
					SharedPreferences sharedPref = getActivity().getSharedPreferences("My Preference", Context.MODE_PRIVATE);
					SharedPreferences.Editor editor = sharedPref.edit();
					  editor.putString("latitude",String.valueOf(locaa.latitude));
					  editor.putString("longitude",String.valueOf(locaa.longitude));
					  editor.commit();
						
					((ProjectActivity)getActivity()).fragment = new SetLocationAlarm();
					((ProjectActivity)getActivity()).addToBackStackFragment(NearByPlaces.this.getResources().getString(R.string.CopyOfSetLocationFragment));
					  
					 /* startActivity(new Intent(NearByPlaces.this, SetLocationAlarm.class));*/
					  
				
				}
				
			}
		});

		
	}
	/*@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View layoutView = inflater.inflate(R.layout.nearbyplaces_fragment, container,false);
		continueButton = (Button) layoutView.findViewById(R.id.continue1);
		
		
		
		return layoutView;
	}*/
	
	
	/*@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		new AsyncTask<Void, Void, Void>() {
			
			ProgressDialog progress;
			@Override
			protected void onPreExecute() {
				// TODO Auto-generated method stub
				super.onPreExecute();
				progress = ProgressDialog.show(NearByPlaces.this, "Loading Map" , "Please Wait...");
			}
			
			@Override
			protected Void doInBackground(Void... params) {
				// TODO Auto-generated method stub
				
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				// TODO Auto-generated method stub
				super.onPostExecute(result);
			
					
				loadMap();
				
				progress.dismiss();
			}
			
		}.execute();
		
		
		
		
		
		
		
		

		
		
		
	
	}
	
	
	
	 private class GeocoderHandler extends Handler {
	        @Override
	        public void handleMessage(Message message) {
	            String locationAddress;
	            switch (message.what) {
	                case 1:
	                    Bundle bundle = message.getData();
	                    locationAddress = bundle.getString("address");
	                    break;
	                default:
	                    locationAddress = null;
	            }
	            displayAddress.setText(locationAddress);
	        }
	    }

	*/
	
	
	
		
		

	
	
	
	
	
	/*@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		super.onDestroyView();
	}
	*/
	
	/*@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		if(item.getItemId()==R.id.home)
		{
			finish();
		}
		
		return super.onOptionsItemSelected(item);
	}*/
}
