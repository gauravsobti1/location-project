package com.example.locationn;



import java.util.Date;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

public class RemAndAlarm extends Activity{

	Button btndatePicker;
	Button btntimePicker;
	Button select;
	DatePickerDialog _date;
	TimePickerDialog _time;
	 TimePickerDialog.OnTimeSetListener timeCallBack;
	 DatePickerDialog.OnDateSetListener dateCall;
	 
	 EditText edt;
	 String reminder;
	 String date,month;
	 String hour,minute;
	 final String year= "2014";
	 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.remandalarm);

		edt= (EditText) findViewById(R.id.editText1);
		
		btndatePicker = (Button)findViewById(R.id.rembutton1);
		btntimePicker = (Button)findViewById(R.id.rembutton2);
		
		btndatePicker.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				_date = new DatePickerDialog(RemAndAlarm.this,dateCall, 2014, 06, 19);
			DatePicker dp=	_date.getDatePicker();
				//dp.setCalendarViewShown(true);
				//dp.setMinDate(19800000);
				Date date= new Date();
			long d	=date.getTime();
				dp.setMinDate(d);
				@SuppressWarnings("deprecation")
				Date date1 = new Date(114,12,31);
				long d1=date1.getTime();
				dp.setMaxDate(d1);	
//			 new Date(System.currentTimeMillis())
				
				
				_date.show();
			
				
				
			}
		});
		
		btntimePicker.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				_time =  new TimePickerDialog(RemAndAlarm.this, timeCallBack, 14, 18, false);
				_time.show();
			}
		});
		
		  timeCallBack = new OnTimeSetListener() {
			
			@Override
			public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
				// TODO Auto-generated method stub
				Toast.makeText(RemAndAlarm.this, "The time selected is : "+hourOfDay+ ":" +minute, Toast.LENGTH_SHORT).show();
				RemAndAlarm.this.hour= hourOfDay+"";
				RemAndAlarm.this.minute= minute+"";
			}
		};
		
		 dateCall = new OnDateSetListener() {
			
			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear,
					int dayOfMonth) {
				// TODO Auto-generated method stub
				Toast.makeText(RemAndAlarm.this, "The date selected is : "+ dayOfMonth +"/" + ++monthOfYear + "/" + year, Toast.LENGTH_SHORT).show();
			RemAndAlarm.this.date=dayOfMonth+"";
			RemAndAlarm.this.month=monthOfYear+""; 
				
			}
		};
	
		
		select= (Button) findViewById(R.id.Select);
		
		
		
		
		select.setOnClickListener(new Button.OnClickListener()
		{

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				reminder=edt.getText()+"";
				Dataaaa dataa= new Dataaaa(RemAndAlarm.this);
				dataa.open();
				dataa.createEntry2(reminder,date, month, hour, minute);
				dataa.close();
				
				Toast.makeText(RemAndAlarm.this, "Reminder,Date and month added", Toast.LENGTH_SHORT).show();
				
				Intent ii= new Intent(RemAndAlarm.this, First.class);
				startActivity(ii);
				finish();
				
				
			}
			
			
			
		});
		
		
		
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		return super.onCreateOptionsMenu(menu);
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		return super.onOptionsItemSelected(item);
	}
	
}
