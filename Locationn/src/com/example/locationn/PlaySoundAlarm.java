package com.example.locationn;

import android.app.Activity;
import android.app.ActionBar;
import android.app.AlarmManager;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.os.Build;
import android.preference.PreferenceManager;

public class PlaySoundAlarm extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_play_sound_alarm);

		TextView name = (TextView) findViewById(R.id.Name);
		TextView message = (TextView) findViewById(R.id.Message);
		Button stopAlarm = (Button) findViewById(R.id.stopAlarm);
		name.setText("Name = "+getIntent().getStringExtra("Name"));
		message.setText("Message = "+getIntent().getStringExtra("Message")+"\n \n" + "Address = " + getIntent().getStringExtra("Addr") );
		
		final AudioManager am = (AudioManager) this.getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
		
		final int ringermode = am.getRingerMode();		
		
		am.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
		
		 SharedPreferences getAlarms = PreferenceManager.
                 getDefaultSharedPreferences(getBaseContext());
		 String alarms = getAlarms.getString("ringtone", "default ringtone");
		 Uri uri = Uri.parse(alarms);
		
		//Uri notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
		final Ringtone r = RingtoneManager.getRingtone(this.getApplicationContext(), uri);
		r.play();
		
		//final MediaPlayer mp = MediaPlayer.create(getApplicationContext(), uri);
		//mp.start();
		stopAlarm.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
			
				r.stop();
				//mp.stop();
				am.setRingerMode(ringermode);
				PlaySoundAlarm.this.finish();
				
			}
		});
		
		
	}

	
}
