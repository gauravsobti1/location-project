package com.example.locationn;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class Dataaaa {
	
	public static final String key_id="id";
	public static final String key_name="name";
	public static final String key_range="range";
	public static final String key_message="message";
	//public static final String key_alarm="alarm";
	//public static final String key_notification="notification";
	public static final String key_longitude="longitude";
	public static final String key_latitude="latitude";
	public static final String key_address="address";
	
	//public static final String key_alarm_id="alarm_id";
	public static final String key_reminder="reminder";
	public static final String key_alarm_date="alarm_date";
	public static final String key_alarm_month="alarm_month";
	public static final String key_alarm_hour="alarm_hour";
	public static final String key_alarm_minute="alarm_minute";
			
			
	private static final String database_name="Location_db";
	private static final String database_table1="Location_table";
	private static final String database_table2="Alarm_table";
	private static final int database_version=1;
	
	private DBhelper ourHelper;
	private  Context ourContext;
	private SQLiteDatabase ourDatabase;
	
	private static class DBhelper extends SQLiteOpenHelper
	{

		public DBhelper(Context context) {
			super(context, database_name, null, database_version);
			// TODO Auto-generated constructor stub
		}

		

		@Override
		public void onUpgrade(SQLiteDatabase arg0, int arg1/* old version of database */, int arg2 /* new version of database */) {
			// TODO Auto-generated method stub
			
			// If new version is greater than older version then only this function will work
			
			arg0.execSQL("DROP TABLE IF EXISTS "+ database_table1);
			arg0.execSQL("DROP TABLE IF EXISTS "+ database_table2);	
			//now we are calling the onCreate function(which we are overriding below and asking it to create the table)
			onCreate(arg0);
		}



		@Override
		public void onCreate(SQLiteDatabase arg0) {
			
			arg0.execSQL("CREATE TABLE "+ database_table1 + " ( " + key_id + " INTEGER PRIMARY KEY AUTOINCREMENT, " + key_name +
					      " TEXT , " + key_range + " TEXT , " + key_message + " TEXT , " + key_latitude + " TEXT , " + key_longitude + " TEXT, " + key_address + " TEXT" + ")"	);
			
			arg0.execSQL("CREATE TABLE " + database_table2 + " ( " + key_id + " INTEGER PRIMARY KEY AUTOINCREMENT, " + key_reminder + " TEXT ," +  key_alarm_date  + " TEXT , "
					      + key_alarm_month  + " TEXT ," + key_alarm_hour + " TEXT ," + key_alarm_minute + " TEXT " +  " )" );
					
			
		}
		
	}
	
	public Dataaaa(Context c)
	{
		ourContext=c;
	}
	public Dataaaa open()
	{	
		ourHelper= new DBhelper(ourContext);
		ourDatabase=ourHelper.getWritableDatabase();
		return this;
	}
	
	public void close()
	{
		ourHelper.close();
	}
	
	public long createEntry1(String name, String range, String message, String latitude, String longitude,String address)
	{
		ContentValues cv = new ContentValues();
		cv.put(key_name,name);
		cv.put(key_range, range);
		cv.put(key_message, message);
		cv.put(key_latitude, latitude);
		cv.put(key_longitude, longitude);
		cv.put(key_address, address);
		return ourDatabase.insert(database_table1, null, cv);
	}
	

	public void deleteEntryT1(String nam,String messa,String addres,String rang)
	{
		ourDatabase.delete(database_table1, key_name+ " = ? AND " + key_range + " = ? AND " +  key_message + " = ? AND "
	                       +  key_address + " = ?" , new String[]{nam,messa,addres,rang});
		
	}
	
	public long createEntry2(String reminder,String alarm_date,String alarm_month, String alarm_hour,String alarm_minute)
	{
		ContentValues cv1= new ContentValues();
		cv1.put(key_reminder, reminder);
		cv1.put(key_alarm_date, alarm_date);
		cv1.put(key_alarm_month, alarm_month);
		cv1.put(key_alarm_hour, alarm_hour);
		cv1.put(key_alarm_minute, alarm_minute);
		return ourDatabase.insert(database_table2, null /* for which entry to be null (Maybe) */, cv1);
	}
	
	public ArrayList<String> getdata2()
	{
		String [] columns= new String[]{key_id,key_reminder,key_alarm_date,key_alarm_month,key_alarm_hour, key_alarm_minute};
		Cursor c = ourDatabase.query(database_table2, columns, null, null, null, null, null);
		ArrayList <String> result=new ArrayList<String>();;
		
		int iID = c.getColumnIndex(key_id);
		int remind=c.getColumnIndex(key_reminder);
		int idate = c.getColumnIndex(key_alarm_date);
		int imonth= c.getColumnIndex(key_alarm_month);
		int ihour= c.getColumnIndex(key_alarm_hour);
		int iminute= c.getColumnIndex(key_alarm_minute);
		
		for(c.moveToFirst(); !c.isAfterLast(); c.moveToNext())
		{
			result.add( c.getString(iID) + " " +c.getString(remind)+ " " + c.getString(idate)+ " " + c.getString(imonth) + " " + c.getString(ihour)+" "
					+ c.getString(iminute));
			
		}
		
		return result;
	}
	
	public  ArrayList<String> getDate()
	{
		String [] columns= new String[]{key_id,key_reminder,key_alarm_date,key_alarm_month,key_alarm_hour, key_alarm_minute};
		Cursor c = ourDatabase.query(database_table2, columns, null, null, null, null, null);
		ArrayList<String> result1= new ArrayList<String>();
		int idate = c.getColumnIndex(key_alarm_date);
		int imonth= c.getColumnIndex(key_alarm_month);
		
		for(c.moveToFirst(); !(c.isAfterLast()); c.moveToNext())
		{
			
			result1.add(c.getString(idate)+" / "+ c.getString(imonth)+" / 2014" );
			
		}
		
		return result1;
		
	}
	
	public  ArrayList<String> getDay()
	{
		String [] columns= new String[]{key_id,key_reminder,key_alarm_date,key_alarm_month,key_alarm_hour, key_alarm_minute};
		Cursor c = ourDatabase.query(database_table2, columns, null, null, null, null, null);
		ArrayList<String> result1= new ArrayList<String>();
		int idate = c.getColumnIndex(key_alarm_date);
		
		
		for(c.moveToFirst(); !(c.isAfterLast()); c.moveToNext())
		{
			
			result1.add(c.getString(idate) );
			
		}
		
		return result1;
		
	}
	
	public  ArrayList<String> getMonth()
	{
		String [] columns= new String[]{key_id,key_reminder,key_alarm_date,key_alarm_month,key_alarm_hour, key_alarm_minute};
		Cursor c = ourDatabase.query(database_table2, columns, null, null, null, null, null);
		ArrayList<String> result1= new ArrayList<String>();
		int imonth = c.getColumnIndex(key_alarm_month);
		
		
		for(c.moveToFirst(); !(c.isAfterLast()); c.moveToNext())
		{
			
			result1.add(c.getString(imonth) );
			
		}
		
		return result1;
		
	}
	
	public  ArrayList<String> getHour()
	{
		String [] columns= new String[]{key_id,key_reminder,key_alarm_date,key_alarm_month,key_alarm_hour, key_alarm_minute};
		Cursor c = ourDatabase.query(database_table2, columns, null, null, null, null, null);
		ArrayList<String> result1= new ArrayList<String>();
		int ihour = c.getColumnIndex(key_alarm_hour);
		
		
		for(c.moveToFirst(); !(c.isAfterLast()); c.moveToNext())
		{
			
			result1.add(c.getString(ihour) );
			
		}
		
		return result1;
		
	}
	
	public  ArrayList<String> getminute()
	{
		String [] columns= new String[]{key_id,key_reminder,key_alarm_date,key_alarm_month,key_alarm_hour, key_alarm_minute};
		Cursor c = ourDatabase.query(database_table2, columns, null, null, null, null, null);
		ArrayList<String> result1= new ArrayList<String>();
		int iminute = c.getColumnIndex(key_alarm_minute);
		
		
		for(c.moveToFirst(); !(c.isAfterLast()); c.moveToNext())
		{
			
			result1.add(c.getString(iminute) );
			
		}
		
		return result1;
		
	}
	
	
	
	public void UpdateTable2(int Row,String reminder, String date, String month, String hour, String minute )
	{
		
		ContentValues cvUpdate= new ContentValues();
		cvUpdate.put(key_reminder, reminder);
		cvUpdate.put(key_alarm_date,date);
		cvUpdate.put(key_alarm_month,month);
		cvUpdate.put(key_alarm_hour,hour);
		cvUpdate.put(key_alarm_minute, minute);
		ourDatabase.update(database_table2, cvUpdate, key_id+ "=" + Row, null);
		
		
	}
	
	public void deleteEntryT2(String reminder, String date, String month, String hour, String minute)
	{
		ourDatabase.delete(database_table2, key_reminder+ "= ? AND " + key_alarm_date + " = ? AND " 
				+ key_alarm_month + " = ? AND " + key_alarm_hour + " = ? AND " 
				+ key_alarm_minute + " = ?" , new String[] {reminder,date,month,hour,minute});
		
	}
	

	
	public  ArrayList<String> getTime()
	{
		String [] columns= new String[]{key_id,key_reminder,key_alarm_date,key_alarm_month,key_alarm_hour, key_alarm_minute};
		Cursor c = ourDatabase.query(database_table2, columns, null, null, null, null, null);
		ArrayList<String> result1= new ArrayList<String>();
		int ihour= c.getColumnIndex(key_alarm_hour);
		int iminute= c.getColumnIndex(key_alarm_minute);
		
		for(c.moveToFirst(); !c.isAfterLast(); c.moveToNext())
		{
			
			result1.add(c.getString(ihour)+" : "+ c.getString(iminute)) ;
			
		}
		
		return result1;
		
	}
	
	
	public  ArrayList<String> getReminder()
	{
		String [] columns= new String[]{key_id,key_reminder,key_alarm_date,key_alarm_month,key_alarm_hour, key_alarm_minute};
		Cursor c = ourDatabase.query(database_table2, columns, null, null, null, null, null);
		ArrayList<String> result1= new ArrayList<String>();
		int remind=c.getColumnIndex(key_reminder);
		
		for(c.moveToFirst(); !c.isAfterLast(); c.moveToNext())
		{
			
			result1.add(c.getString(remind)) ;
			
		}
		
		return result1;
		
	}
	
	public  ArrayList<String> getName()
	{
		String [] columns= new String[]{key_id,key_name,key_range,key_message,key_latitude, key_longitude,key_address};
		Cursor c = ourDatabase.query(database_table1, columns, null, null, null, null, null);
		ArrayList<String> result2= new ArrayList<String>();
		int name=c.getColumnIndex(key_name);
		
		for(c.moveToFirst(); !c.isAfterLast(); c.moveToNext())
		{
			
			result2.add(c.getString(name)) ;
			
		}
		
		return result2;
		
	}
	
	
	
	public  ArrayList<String> getRange()
	{
		String [] columns= new String[]{key_id,key_name,key_range,key_message,key_latitude, key_longitude,key_address};
		Cursor c = ourDatabase.query(database_table1, columns, null, null, null, null, null);
		ArrayList<String> result2= new ArrayList<String>();
		int range=c.getColumnIndex(key_range);
		
		for(c.moveToFirst(); !c.isAfterLast(); c.moveToNext())
		{
			
			result2.add(c.getString(range)) ;
			
		}
		
		return result2;
		
	}
	
	public  ArrayList<String> getMessage()
	{
		String [] columns= new String[]{key_id,key_name,key_range,key_message,key_latitude, key_longitude,key_address};
		Cursor c = ourDatabase.query(database_table1, columns, null, null, null, null, null);
		ArrayList<String> result2= new ArrayList<String>();
		int message=c.getColumnIndex(key_message);
		
		for(c.moveToFirst(); !c.isAfterLast(); c.moveToNext())
		{
			
			result2.add(c.getString(message)) ;
			
		}
		
		return result2;
		
	}
	
	

	
	
	public  ArrayList<String> getkeylatitude()
	{
		String [] columns= new String[]{key_id,key_name,key_range,key_message,key_latitude, key_longitude,key_address};
		Cursor c = ourDatabase.query(database_table1, columns, null, null, null, null, null);
		ArrayList<String> result2= new ArrayList<String>();
		int latitude_data=c.getColumnIndex(key_latitude);
		
		for(c.moveToFirst(); !c.isAfterLast(); c.moveToNext())
		{
			
			result2.add(c.getString(latitude_data)) ;
			
		}
		
		return result2;
		
	}
	
	
	public  ArrayList<String> getkeylongitude()
	{
		String [] columns= new String[]{key_id,key_name,key_range,key_message,key_latitude, key_longitude,key_address};
		Cursor c = ourDatabase.query(database_table1, columns, null, null, null, null, null);
		ArrayList<String> result2= new ArrayList<String>();
		int longitude_data=c.getColumnIndex(key_longitude);
		
		for(c.moveToFirst(); !c.isAfterLast(); c.moveToNext())
		{
			
			result2.add(c.getString(longitude_data)) ;
			
		}
		
		return result2;
		
	}
	
	
	public  ArrayList<String> getkeyAddress()
	{
		String [] columns= new String[]{key_id,key_name,key_range,key_message,key_latitude, key_longitude,key_address};
		Cursor c = ourDatabase.query(database_table1, columns, null, null, null, null, null);
		ArrayList<String> result2= new ArrayList<String>();
		int address_index=c.getColumnIndex(key_address);
		
		for(c.moveToFirst(); !c.isAfterLast(); c.moveToNext())
		{
			
			result2.add(c.getString(address_index)) ;
			
		}
		
		return result2;
		
	}
	
	
	
}
