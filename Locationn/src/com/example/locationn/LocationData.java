package com.example.locationn;

public class LocationData {

	double latitude;
	double longitude;
	String nameHeading;
	String message;
	String Range;
	String address;
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	long time;
	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	long id;
	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getLatitude() {
		return latitude;
	}
	
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	
	public double getLongitude() {
		return longitude;
	}
	
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
	public String getNameHeading() {
		return nameHeading;
	}
	
	public void setNameHeading(String nameHeading) {
		this.nameHeading = nameHeading;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getRange() {
		return Range;
	}
	
	public void setRange(String range) {
		Range = range;
	}
	
	
	
}
