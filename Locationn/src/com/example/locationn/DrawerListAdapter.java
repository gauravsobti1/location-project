package com.example.locationn;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;



public class DrawerListAdapter extends BaseAdapter {

	
	private Context context;
	private ArrayList<String> resource;

	public DrawerListAdapter(Context c, ArrayList<String> resource) {
		
		context=c;
		
		this.resource= resource;
		
	}
	
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return resource.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View row = inflater.inflate(R.layout.drawer_listview, parent,false);
		
		TextView drawerText = (TextView) row.findViewById(R.id.drawer_text);
		
		drawerText.setText(resource.get(position));
		
		
		
		return row;
	}
	
}