package com.example.locationn;

import android.app.AlarmManager;
import android.app.Fragment;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class CopyOfSetLocationAlarm extends Fragment{
	
	
	private double latitude;
	private double longitude;
	private Button okButton;
	private EditText enterName;
	private EditText enterMessage;
	private EditText enterRange;
	private EditText enterTime;
	private TextView hint1;
	private TextView hint2;
	private String address;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View layoutView= inflater.inflate(R.layout.set_location_alarm, container,false);
		SharedPreferences sharedPref = getActivity().getSharedPreferences("My Preference", Context.MODE_PRIVATE);
		try {
			latitude = Double.parseDouble(sharedPref.getString("latitude", ""));
			longitude = Double.parseDouble(sharedPref.getString("longitude", ""));
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		okButton = (Button) layoutView.findViewById(R.id.button1);
		enterName = (EditText) layoutView.findViewById(R.id.enterNameOfAlarm);
		enterMessage = (EditText) layoutView.findViewById(R.id.enterMessage);
		enterRange = (EditText) layoutView.findViewById(R.id.enterRange);
		enterTime = (EditText) layoutView.findViewById(R.id.enterTime);
		hint1 = (TextView ) layoutView.findViewById(R.id.textView4);
		hint2 = (TextView ) layoutView.findViewById(R.id.textView6);
		return layoutView;
	}
	
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		hint1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				Toast.makeText(getActivity(), "Enter the range in kms", Toast.LENGTH_LONG).show();
				
			}
		});
		
hint2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				Toast.makeText(getActivity(), "Enter the time in hours after which to start searching for updates", Toast.LENGTH_LONG).show();
				
			}
		});
		
		
		okButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				if(moveForward())
				{
					
					
				
				final LocationData locData = new LocationData();
				AddressFromLocation.getAddressFromLocation(latitude, longitude, getActivity(), new GeocoderHandler());
				locData.setNameHeading(enterName.getText().toString());
				locData.setMessage( enterMessage.getText().toString());
				locData.setRange(enterRange.getText().toString());
				locData.setTime(System.currentTimeMillis()+ (Integer.parseInt(enterTime.getText().toString()))*60*60*1000);
				Log.e("Time after update = ", locData.getTime()+"");
				locData.setLatitude(latitude);
				locData.setLongitude(longitude);
				
				new AsyncTask<Void, Void, Void>() {
					ProgressDialog progress;
					protected void onPreExecute() {
						
						progress= ProgressDialog.show(getActivity(), " Adding Alarm ", "Please Wait ...");
						
					}
					@Override
					protected Void doInBackground(Void... params) {
						try {
							Thread.sleep(5000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						return null;
					}
					
					@Override
					protected void onPostExecute(Void result) {
						// TODO Auto-generated method stub
						super.onPostExecute(result);
						try {
							Log.v("Address = ", address);
							locData.setAddress(address);
							DBhelper db = new DBhelper(getActivity());
							db.createLocationTable(locData);
							db.closeDB();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							Toast.makeText(getActivity().getApplicationContext(), "Sorry! Couldn't add alarm. Please try again!", Toast.LENGTH_LONG).show();
						}
						((ProjectActivity)getActivity()).fragment = new Home_Page();
						((ProjectActivity)getActivity()).addToBackStackFragment(getActivity().getResources().getString(R.string.HomePageFragment));
						progress.dismiss();
						
					}
				}.execute();
				
				
					
				/*AlarmManager alarmMgr = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
			      
				Intent storeData = new Intent(getActivity(), StoreDataReceiver.class);
				storeData.putExtra("double_latitude", latitude);
				storeData.putExtra("double_longitude", longitude);
				storeData.putExtra("string_name", enterName.getText().toString());
				storeData.putExtra("string_message", enterMessage.getText().toString());
				storeData.putExtra("string_range", enterRange.getText().toString());
				storeData.putExtra("int_time", Integer.parseInt(enterTime.getText().toString()));
				
				
			      PendingIntent pendingIntent =
			               PendingIntent.getBroadcast(getActivity(), 0, storeData, 0);*/
			      
			      // use inexact repeating which is easier on battery (system can phase events and not wake at exact times)
			     /* alarmMgr.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + 30000,
			               AlarmManager.INTERVAL_FIFTEEN_MINUTES, pendingIntent);*/
			      
			   /*  alarmMgr.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + (Integer.parseInt(enterTime.getText().toString()))*1*60*1000 , pendingIntent);
				Toast.makeText(getActivity(), "Data added to database", Toast.LENGTH_SHORT).show();*/
			     
				/*((ProjectActivity)getActivity()).fragment = new Home_Page();
				((ProjectActivity)getActivity()).addToBackStackFragment(getActivity().getResources().getString(R.string.HomePageFragment));*/
				
			     
				}
				else
					Toast.makeText(getActivity(), "Please fill all the required fields", Toast.LENGTH_LONG).show();
				
			}

			private boolean moveForward() {
				
				if(enterName.getText().toString().trim().length() >0 && enterMessage.getText().toString().trim().length() >0 &&  
						enterRange.getText().toString().trim().length() >0	&&
						enterTime.getText().toString().trim().length() >0
						)
				return true;
			else
				return false;
		}
			
		});
		
		
	}
	
	
	
	 private class GeocoderHandler extends Handler {
	        @Override
	        public void handleMessage(Message message) {
	            String locationAddress;
	            switch (message.what) {
	                case 1:
	                    Bundle bundle = message.getData();
	                    locationAddress = bundle.getString("address");
	                    break;
	                default:
	                    locationAddress = null;
	            }
	            address= locationAddress;
	        }
	    }


}
