package com.example.locationn;

import java.util.Date;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

public class EditAlarm extends Activity{

	Button btndatePicker;
	Button btntimePicker;
	Button editbutton;
	DatePickerDialog _date;
	TimePickerDialog _time;
	 TimePickerDialog.OnTimeSetListener timeCallBack;
	 DatePickerDialog.OnDateSetListener dateCall;
	int row;
	 EditText edt;
	 String reminder;
	 String date,month;
	 String hour,minute;
	 final String year= "2014";
	 
	@Override
		protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.edit);
		
		Intent myintent= getIntent();
		row=myintent.getIntExtra("Row", 0);
		 
		
		 
		 edt= (EditText) findViewById(R.id.editText1);
		 Dataaaa da= new Dataaaa(EditAlarm.this);
		 Log.e("Opening database first", " Opening");
		 da.open();
		 
		 reminder=(da.getReminder()).get(row-1);
		 
		 da.close();
		 Log.e("Reminder= ", reminder);
				edt.setText(reminder);
			
			btndatePicker = (Button)findViewById(R.id.rembutton1);
			btntimePicker = (Button)findViewById(R.id.rembutton2);
			
			btndatePicker.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					_date = new DatePickerDialog(EditAlarm.this,dateCall, 2014, 06, 19);
				DatePicker dp=	_date.getDatePicker();
					//dp.setCalendarViewShown(true);
					//dp.setMinDate(19800000);
					Date date= new Date();
				long d	=date.getTime();
					dp.setMinDate(d);
					@SuppressWarnings("deprecation")
					Date date1 = new Date(114,12,31);
					long d1=date1.getTime();
					dp.setMaxDate(d1);	
					
//				 new Date(System.currentTimeMillis())
					
					
					_date.show();
				
					
					
				}
			});
			
			btntimePicker.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					_time =  new TimePickerDialog(EditAlarm.this, timeCallBack, 14, 18, false);
					_time.show();
				}
			});
			
			  timeCallBack = new OnTimeSetListener() {
				
				@Override
				public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
					// TODO Auto-generated method stub
					Toast.makeText(EditAlarm.this, "The time selected is : "+hourOfDay+ ":" +minute, Toast.LENGTH_SHORT).show();
					EditAlarm.this.hour= hourOfDay+"";
					EditAlarm.this.minute= minute+"";
				}
			};
			
			 dateCall = new OnDateSetListener() {
				
				@Override
				public void onDateSet(DatePicker view, int year, int monthOfYear,
						int dayOfMonth) {
					// TODO Auto-generated method stub
					Toast.makeText(EditAlarm.this, "The date selected is : "+ dayOfMonth +"/" + ++monthOfYear + "/" + year, Toast.LENGTH_SHORT).show();
				EditAlarm.this.date=dayOfMonth+"";
				EditAlarm.this.month=monthOfYear+""; 
					
				}
			};

			
			editbutton= (Button) findViewById(R.id.editValue);
			
			
			
			
			editbutton.setOnClickListener(new Button.OnClickListener()
			{

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					
					if(date!=null && month!=null && hour!=null && minute!=null )
					{Log.e("Edit button= ", "Edit Button");
					reminder=edt.getText()+"";
					Dataaaa dataa= new Dataaaa(EditAlarm.this);
					dataa.open();
					dataa.UpdateTable2(row, reminder, date, month, hour, minute);
					dataa.close();
					
					Toast.makeText(EditAlarm.this, "Reminder,Date and month of row = "+ row +  "updated", Toast.LENGTH_SHORT).show();
					
					Intent ii= new Intent(EditAlarm.this, First.class);
					startActivity(ii);
					finish();
					}
					else
					{
						Toast.makeText(EditAlarm.this, "Proper value not added! Try again!", Toast.LENGTH_SHORT).show();
						Intent thisIntent= getIntent();
						startActivity(thisIntent);
						finish();
					}
					
				}
				
				
				
			});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		
		super.onBackPressed();
		
	}
	

}
