package com.example.locationn;

import java.util.ArrayList;














import com.example.location.update.SettingsActivity;
import com.example.location.update.TypesOfPlacesFrag;

import android.app.Activity;
import android.app.AlertDialog;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;

public class ProjectActivity extends ActionBarActivity{

	
	
	private ActionBarDrawerToggle mDrawerToggle;
	private DrawerLayout mDrawerLayout;
	private RelativeLayout leftdrawer;
	private ListView drawerList;
	private ArrayList<String> listviewtext;
	public Fragment fragment;
	public FragmentManager fragmentManager;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.project_activity);
		
		leftdrawer= (RelativeLayout) findViewById(R.id.whatYouWantInLeftDrawer);
		mDrawerLayout= (DrawerLayout) findViewById(R.id.drawer_layout);
		drawerList= (ListView) findViewById(R.id.drawer1);
		listviewtext= new ArrayList<String>(); 
		listviewtext.add("Home Page");
		listviewtext.add("Location Alarm");
		listviewtext.add("Map Alarm");
		listviewtext.add("Nearby Places");
		listviewtext.add("Settings");
		
		
		

		drawerList.setAdapter(new DrawerListAdapter(ProjectActivity.this, listviewtext));
		fragmentManager = getSupportFragmentManager();
		
		mDrawerToggle = new ActionBarDrawerToggle(this, /* host Activity */
				mDrawerLayout, /* DrawerLayout object */
				R.string.drawer_open, /* "open drawer" description */
				R.string.drawer_close /* "close drawer" description */
				) {

					/** Called when a drawer has settled in a completely closed state. */
					public void onDrawerClosed(View view) {
						super.onDrawerClosed(view);
						//getActionBar().setTitle(mTitle);
						invalidateOptionsMenu();
					}

					/** Called when a drawer has settled in a completely open state. */
					public void onDrawerOpened(View drawerView) {
						super.onDrawerOpened(drawerView);
						//getActionBar().setTitle(mDrawerTitle);
						invalidateOptionsMenu();
					}
				};

				// Set the drawer toggle as the DrawerListener
				mDrawerLayout.setDrawerListener(mDrawerToggle);
				drawerList.setOnItemClickListener(new DrawerItemClickListener());
				ActionBar actionbar = getSupportActionBar();
				actionbar.setDisplayHomeAsUpEnabled(true);
				actionbar.setDisplayShowTitleEnabled(true);
				/*actionbar.setIcon(android.R.color.transparent);*/
				/*Drawable actionbarbg = getResources().getDrawable( R.drawable.actionbar_bg );
				actionbar.setBackgroundDrawable(actionbarbg);
				*/
				if(savedInstanceState==null)
					selectItem(0);
	}
	
	
	public class DrawerItemClickListener implements
	ListView.OnItemClickListener {

@Override
public void onItemClick(AdapterView<?> parent, View view, int position,
		long id) {
	selectItem(position);
}

}
	
	public void addToBackStackFragment(String backstack_name)
	{
		if (fragment != null ) {
			Log.e("Value of fragment = ", fragment.toString());
			
			fragmentManager.beginTransaction()
					.replace(R.id.container_relativelayout, fragment,backstack_name).addToBackStack(backstack_name).commit();
		}
		
	}

public void selectItem(int position) {

// update the main content by replacing fragments

String backstack_name=null;

switch (position) {
case 0:
	fragment = new Home_Page();
	/*backstack_name=getResources().getString(R.string.HomePageFragment);*/
	backstack_name="class com.example.locationn.Home_Page";
	break;

case 1:
	fragment = new LocationAlarm();
	backstack_name= ProjectActivity.this.getResources().getString(R.string.GoToLocationFragment);
	break;
	
case 3:
	fragment = new TypesOfPlacesFrag();
	backstack_name= ProjectActivity.this.getResources().getString(R.string.GoToLocationFragment);
	break;

case 4:
	startActivity(new Intent(ProjectActivity.this,SettingsActivity.class));
	break;
	
case 2:
	
	LocationManager loc = (LocationManager) ProjectActivity.this.getSystemService(Context.LOCATION_SERVICE);
	if(!loc.isProviderEnabled(LocationManager.GPS_PROVIDER))
	{
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                ProjectActivity.this);
		alertDialogBuilder
    .setTitle("GPS Not Enabled")
    .setMessage("Do you want to turn on GPS?")
    .setPositiveButton("Turn On GPS", new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) { 
        	Intent callGPSSettingIntent = new Intent(
                    android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(callGPSSettingIntent);
        }
     })
    .setNegativeButton("Exit", new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) { 
        	dialog.cancel();
        }
     })
    .setIcon(android.R.drawable.ic_dialog_alert)
     .show();
		mDrawerLayout.closeDrawer(leftdrawer);
	}
	else
	{	
		if(fragment==null)
	{fragment = new NearByPlaces();
	backstack_name= ProjectActivity.this.getResources().getString(R.string.NearByPlacesFragment);
	}
		
		/*startActivity(new Intent(ProjectActivity.this, NearByPlaces.class));*/
	}
default:
	break;
}



if (fragment != null ) {
	Log.e("Value of fragment = ", fragment.toString());
	/*Log.v("back_stack name = ", backstack_name );
	Log.i("back_stack name = ", backstack_name );*/
	fragmentManager.beginTransaction().replace(R.id.container_relativelayout, fragment, backstack_name).addToBackStack(backstack_name).commit();
	/*fragmentManager.beginTransaction()
			.replace(R.id.container_relativelayout, fragment,backstack_name).addToBackStack(backstack_name).commit();
*/	

	
	drawerList.setItemChecked(position, true);
	drawerList.setSelection(position);

	mDrawerLayout.closeDrawer(leftdrawer);
	fragment=null;
} 
}


@Override
public void onCreateContextMenu(ContextMenu menu, View v,
		ContextMenuInfo menuInfo) {
	
	getMenuInflater().inflate(R.menu.main, menu);
	
	super.onCreateContextMenu(menu, v, menuInfo);
}

@Override
public boolean onOptionsItemSelected(MenuItem item) {
	
	if (mDrawerToggle.onOptionsItemSelected(item)) {
		return true;
	}
	
	
	return super.onOptionsItemSelected(item);
}

@Override
protected void onPostCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onPostCreate(savedInstanceState);
	
	mDrawerToggle.syncState();
	
}

@Override
public void onBackPressed() {
	// TODO Auto-generated method stub
	
	Fragment currentFragment = fragmentManager.findFragmentById(R.id.container_relativelayout);
	Log.e("Value of class= ", (new Home_Page()).getClass().toString());
	
	if(currentFragment.getTag().equals((new Home_Page()).getClass().toString()))
    {
    	Log.v("Inside if condition","equal to home page");
    	 this.finish();
    	/*fragmentManager.popBackStack();*/
    }	
    	else if(currentFragment.getTag().equals((new LocationAlarm()).getClass().toString())|| currentFragment.getTag().equals((new NearByPlaces()).getClass().toString()))
    	{
    		Log.v("Inside if equal to settings", "Inside if equal to settings");
    		fragmentManager.popBackStack(getResources().getString(R.string.HomePageFragment), 0);
    	}
    	else if(currentFragment.getTag().equals((new SetLocationAlarm()).getClass().toString()))
    	{
    		Log.v("Inside if equal to settings", "Inside if equal to settings");
    		fragmentManager.popBackStack(getResources().getString(R.string.GoToLocationFragment), 0);
    	}
	
    	else if(currentFragment.getTag().equals((new CopyOfSetLocationAlarm()).getClass().toString()))
    	{
    		Log.v("Inside if equal to settings", "Inside if equal to settings");
    		fragmentManager.popBackStack(getResources().getString(R.string.HomePageFragment), 0);
    	}
}


}
