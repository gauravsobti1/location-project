package com.example.locationn;



import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;




import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnCreateContextMenuListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

public class First extends Activity implements LocationListener {
	
	Button b1,b2,b3;
	double longit,latitu;
	Location l;
	Context context;
	public static String day5,month5,hour5,minute5,name5,message5 ;
	
	
	
@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
	setContentView(R.layout.first);
	
	Drawable actionbarbg = getResources().getDrawable( R.drawable.actionbar_bg );
	getActionBar().setBackgroundDrawable(actionbarbg);
	
	
	
	Dataaaa diii = new Dataaaa(this);
	diii.open();
	int size=diii.getkeylatitude().size();
	diii.close();
	if(size!=0)
	{
		
	Intent ser= new Intent(First.this,NotificationService.class);
	startService(ser);
	}
	
	
	ArrayList<String> day= new ArrayList<String>();
	ArrayList<String> month= new ArrayList<String>();
	ArrayList<String> hour= new ArrayList<String>();
	ArrayList<String> minute= new ArrayList<String>();
	//ArrayList<String> nammmmmme= new ArrayList<String>();
	ArrayList<String> messssssage= new ArrayList<String>();
	Dataaaa dd = new Dataaaa(First.this);
	dd.open();
	
	day=dd.getDay();
	month=dd.getMonth();
	hour=dd.getHour();
	minute=dd.getminute();
	//nammmmmme=dd.getName();
	messssssage=dd.getReminder();
	dd.close();
	
	
	
	
	
	if(day!=null)
	{if(day.toString()!="[]" && month.toString()!="[]")
	{
	String[] day1= new String[(day.size())];
	Log.v("Data= ",day.toString());
	String[] month1= new String[(month.size())];
	Log.v("Month= ",month.toString());
	String[] hour1= new String[(hour.size())];
	String[] minute1= new String[(minute.size())];
	day1= day.toArray(day1);
	month1= month.toArray(month1);
	hour1=hour.toArray(hour1);
	minute1=minute.toArray(minute1);
	
	day5=day.get(0);
	month5=month.get(0);
	hour5=hour.get(0);
	minute5=minute.get(0);
	//name5=nammmmmme.get(0);
	message5= messssssage.get(0);
	
	
	int day2= Integer.parseInt(day1[0]);
	Log.e("day2= ",day2 +"");
	int month2= Integer.parseInt(month1[0]);
	Log.e("month2= ",month2 +"");
	int hour2= Integer.parseInt(hour1[0]);
	Log.e("hour= ",hour2 +"");
	int minute2= Integer.parseInt(minute1[0]);
	Log.e("minute= ",minute2 +"");
	@SuppressWarnings("deprecation")
	Calendar d= Calendar.getInstance();
	Log.e("Date= ", d+"");
	int current_date=d.get(Calendar.DATE);
	int current_month=d.get(Calendar.MONTH)+1;
	int current_hour_of_day=d.get(Calendar.HOUR_OF_DAY);
	int current_minute= d.get(Calendar.MINUTE);
	Log.e("Current date= ",current_date +"");
	Log.e("Current month= ",current_month +"");
	Log.e("Current hour of day= ", current_hour_of_day+"");
	Log.e("Current minute= ",current_minute +"");
	
	
	GregorianCalendar currentDay=new  GregorianCalendar (2014,current_month,current_date,current_hour_of_day,current_minute,0);
	
	GregorianCalendar nextDay=new  GregorianCalendar (2014,month2,day2,hour2,minute2,0);

	long diff_in_ms= nextDay.getTimeInMillis()-currentDay. getTimeInMillis();
	Log.e("difference= ",System.currentTimeMillis()+ diff_in_ms+"");
	
	Intent newinten = new Intent(First.this,Alarm.class);
	PendingIntent pendingintent= PendingIntent.getActivity(First.this, 2, newinten, PendingIntent.FLAG_CANCEL_CURRENT);
	AlarmManager am = (AlarmManager)getSystemService(ALARM_SERVICE);
	am.set(AlarmManager.RTC_WAKEUP,System.currentTimeMillis()+diff_in_ms , pendingintent); 
	
	//System.currentTimeMillis()+
	}
	}
	b1=(Button)findViewById(R.id.button1);
	b2=(Button)findViewById(R.id.button2);
	b3=(Button)findViewById(R.id.button3);
	
	
	
	b1.setOnClickListener(new Button.OnClickListener()
	{

		

		private LocationManager locationManager;

		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			
			 locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			 Toast.makeText(First.this,"locationManager = " +locationManager.toString() , Toast.LENGTH_SHORT).show();
		 locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, First.this);
		//LocationListener lli= new MyLocationListener();
		//locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,0,0, lli);
			//locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		
	        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
	        	

//	          {
	        	if(locationManager!=null)
	        		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0,100, First.this);
	        		
	        		l=  locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
	        		
	        		if(l!=null)
	        		{
	        			Log.e("Nothing","is present");
	        	
	        		
	        	 longit=l.getLongitude();
	        	 latitu=l.getLatitude();
	        	 Log.e("latitude =  longitude =  ", latitu + "    " +  longit );
	        	 Intent myIntent = new Intent(First.this,Locatio.class);
		        	myIntent.putExtra("long", longit);
		        	myIntent.putExtra("lat", latitu);
		        	
		            startActivity(myIntent);
		            finish();
	        		}
	        		
	        		else
	        			Toast.makeText(First.this,"l is null" , Toast.LENGTH_SHORT).show();
	        		
	      //      }
	         /*   catch (Exception e)
	            {
	       	e.printStackTrace();
	          }*/
	            
	        	
	        }/*else{
	            showGPSDisabledAlertToUser();
	        }*/
	    }

	    private void showGPSDisabledAlertToUser(){
	        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(First.this);
	        alertDialogBuilder.setMessage("NETWORK is disabled in your device. Would you like to enable it?")
	        .setCancelable(false)
	        .setPositiveButton("Goto Settings Page To Enable NETWORK",
	                new DialogInterface.OnClickListener(){
	            public void onClick(DialogInterface dialog, int id){
	                Intent callGPSSettingIntent = new Intent(
	                        android.provider.Settings.ACTION_NETWORK_OPERATOR_SETTINGS);
	                startActivity(callGPSSettingIntent);
	            }
	        });
	        alertDialogBuilder.setNegativeButton("Cancel",
	                new DialogInterface.OnClickListener(){
	            public void onClick(DialogInterface dialog, int id){
	                dialog.cancel();
	            }
	        });
	        AlertDialog alert = alertDialogBuilder.create();
	        alert.show();
	    }
		
		

			
	});
	
	
	
	
	// Reminder and alarm onclick listener
	
	
	b2.setOnClickListener(new Button.OnClickListener()
	{

		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			
			Intent intent2= new Intent(First.this,RemAndAlarm.class);
			startActivity(intent2);
			finish();
			
		}
		
	});
	
	b3.setOnClickListener(new Button.OnClickListener()
	{

		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			Intent i3 = new Intent(First.this,ViewAndEdit.class);
			startActivity(i3);
			finish();
		}
		
	});
	
	
}


/*class MyLocationListener implements LocationListener
{

	@Override
	public void onLocationChanged(Location arg0) {
		if(arg0!=null)
		{
			longit= arg0.getLongitude();
			latitu=arg0.getLatitude();
			//Toast.makeText(First.this," l is zero", Toast.LENGTH_LONG).show();
		}
		
		if(arg0==null)
		{
			Toast.makeText(First.this," l is zero", Toast.LENGTH_LONG).show();
		}
			
			//Toast.makeText(First.this, longit+" "+latitu+" ", Toast.LENGTH_LONG).show();
		Intent myIntent = new Intent(First.this,Locatio.class);
	   	myIntent.putExtra("long", longit);
	   	myIntent.putExtra("lat", latitu);
	   	
	       startActivity(myIntent);
	       finish();
		
			
		
		
	}

	@Override
	public void onProviderDisabled(String arg0) {
		// TODO Auto-generated method stub
		 AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(First.this);
	        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
	        .setCancelable(false)
	        .setPositiveButton("Goto Settings Page To Enable GPS",
	                new DialogInterface.OnClickListener(){
	            public void onClick(DialogInterface dialog, int id){
	                Intent callGPSSettingIntent = new Intent(
	                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
	                startActivity(callGPSSettingIntent);
	            }
	        });
	        alertDialogBuilder.setNegativeButton("Cancel",
	                new DialogInterface.OnClickListener(){
	            public void onClick(DialogInterface dialog, int id){
	                dialog.cancel();
	            }
	        });
	        AlertDialog alert = alertDialogBuilder.create();
	        alert.show();
	}

	@Override
	public void onProviderEnabled(String arg0) {
	// TODO Auto-generated method stub
		Intent myIntent = new Intent(First.this,Locatio.class);
	   	myIntent.putExtra("long", longit);
	   	myIntent.putExtra("lat", latitu);
	   	
	       startActivity(myIntent);
	       finish();
		l= locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		if(l==null)
		{
			Log.e("gsdgdsgs","segfsdgsdg");
		}
		else
			{longit= l.getLongitude()+"";
			latitu= l.getLatitude()+"";
		Intent myIntent = new Intent(First.this,Locatio.class);
   	myIntent.putExtra("long", longit);
   	myIntent.putExtra("lat", latitu);
   	
       startActivity(myIntent);
       finish();}
		
	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		// TODO Auto-generated method stub
		
	}

}*/
@Override
public boolean onCreateOptionsMenu(Menu menu) {

	// Inflate the menu; this adds items to the action bar if it is present.
	getMenuInflater().inflate(R.menu.main, menu);
	return true;
}

@Override
public boolean onOptionsItemSelected(MenuItem item) {
	// Handle action bar item clicks here. The action bar will
	// automatically handle clicks on the Home/Up button, so long
	// as you specify a parent activity in AndroidManifest.xml.
	int id = item.getItemId();
	if (id == R.id.action_settings) {
		return true;
	}
	return super.onOptionsItemSelected(item);
}


@Override
public void onLocationChanged(Location location) {
	
	Toast.makeText(First.this.getApplicationContext(), "Location = "+ location.getLatitude() + " " + location.getLongitude(), Toast.LENGTH_SHORT).show();
	
}


@Override
public void onStatusChanged(String provider, int status, Bundle extras) {
	// TODO Auto-generated method stub
	
}


@Override
public void onProviderEnabled(String provider) {
	// TODO Auto-generated method stub
	
}


@Override
public void onProviderDisabled(String provider) {
	// TODO Auto-generated method stub
	
}

}
