package com.example.locationn;



public class MyBean 
{
	String Id,Name,Range,Message,Notification,Alarm,Longitude,Latitude;
	
	public MyBean()
	{}
	
	public MyBean(String Id,String Name,String Range,String Message,String Notification,String Alarm,String Longitude, String Latitude)
	{
		this.Id=Id;
		this.Name=Name;
		this.Range=Range;
		this.Message=Message;
		this.Notification=Notification;
		this.Alarm=Alarm;
		this.Longitude=Longitude;
		this.Latitude=Latitude;
	}

	public String getLatitude() {
		return Latitude;
	}

	public void setLatitude(String latitude) {
		Latitude = latitude;
	}

	public String getId() {
		return Id;
	}

	public void setId(String Id) {
		this.Id = Id;
	}

	public String getName() {
		return Name;
	}

	public void setName(String Name) {
		this.Name = Name;
	}

	public String getRange() {
		return Range;
	}

	public void setRange(String Range) {
		this.Range = Range;
	}

	public String getMessage() {
		return Message;
	}

	public void setMessage(String Message) {
		this.Message = Message;
	}

	public String getNotification() {
		return Notification;
	}

	public void setNotification(String Notification) {
		this.Notification = Notification;
	}

	public String getAlarm() {
		return Alarm;
	}

	public void setAlarm(String Alarm) {
		this.Alarm = Alarm;
	}

	public String getLongitude() {
		return Longitude;
	}

	public void setLongitude(String Longitude) {
		this.Longitude = Longitude;
	}
	
	
	
}
