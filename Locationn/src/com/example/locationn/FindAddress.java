package com.example.locationn;


import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

public class FindAddress  {
	
	String address,postalcode,city,result;
	//String lat,longitude;
	String urlString=null;
	 public volatile boolean parsingComplete = true;
	 public FindAddress(String url){
	      this.urlString = url;
	   }

	 public String getresult()
	 {
		 return result;
	 }

	/*public String getLat() {
		return lat;
	}

	public String getLongitude() {
		return longitude;
	}*/

	
	
	public void readAndParseJSON(String in) {
	      try {
	         JSONObject reader = new JSONObject(in);
	       //  Log.i("String= ", reader.toString());
	         JSONObject response  = reader.getJSONObject("response");
	         //response.has("venues");
	       //  Log.i("Data= ",response.toString());
	         JSONArray venues=response.getJSONArray("venues");
	       //  Log.i("Venue= ",venues.toString());
	         
	         JSONObject location= venues.getJSONObject(1);
	         
	       //  Log.i("Location= ",location.toString());
	         
	         JSONObject location1= location.getJSONObject("location");
	         Log.i("Location1= ",location1.toString());
	         
	         city = location1.getString("city");

	         
	         //postalcode = location1.getString("postalCode");

	         address = location1.getString("address");
	         
	         result= address + "\n" + "\n" + city;
	         parsingComplete = false;



	        } catch (Exception e) {
	           // TODO Auto-generated catch block
	           e.printStackTrace();
	        }

	   }
	   public void fetchJSON(){
	      Thread thread = new Thread(new Runnable(){
	         @Override
	         public void run() {
	         try {
	            URL url = new URL(urlString);
	            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	            conn.setReadTimeout(10000 /* milliseconds */);
	            conn.setConnectTimeout(15000 /* milliseconds */);
	            conn.setRequestMethod("GET");
	            conn.setDoInput(true);
	            // Starts the query
	            conn.connect();
	         InputStream stream = conn.getInputStream();

	      String data = convertStreamToString(stream);

	      readAndParseJSON(data);
	         stream.close();

	         } catch (Exception e) {
	            e.printStackTrace();
	         }
	         }
	      });

	       thread.start(); 		
	   }
	   static String convertStreamToString(java.io.InputStream is) {
	      java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
	      return s.hasNext() ? s.next() : "";
	   }

	
	   
	   
}
