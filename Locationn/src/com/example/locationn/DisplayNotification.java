package com.example.locationn;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.os.Build;

public class DisplayNotification extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_display_notification);

		TextView name = (TextView) findViewById(R.id.Name);
		TextView message = (TextView) findViewById(R.id.Message);
		name.setText("Name = "+getIntent().getStringExtra("Name"));
		message.setText("Message = "+getIntent().getStringExtra("Message")+"\n \n" + "Address = " + getIntent().getStringExtra("Addr") );
		
	}



	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

@Override
public void onBackPressed() {
	// TODO Auto-generated method stub
	this.finish();
}
	

}
