package com.example.locationn;


import java.util.ArrayList;
import java.util.Arrays;


import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.graphics.YuvImage;
import android.util.Log;

@SuppressLint("NewApi")
public class DBLogic 
{
	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "LocationApp.db";
	
	
	
	private static final String TABLE_NAME = "LocationApp";
	private static final String KEY_ID = "id";
	private static final String KEY_NAME = "Name";
	private static final String KEY_RANGE = "Range";
	private static final String KEY_MESSAGE = "Message";
	private static final String KEY_ALARM = "Alarm";
	private static final String KEY_NOTIFICATION = "Notification";
	private static final String KEY_LONGITUDE = "longitude";
	private static final String KEY_LATITUDE = "latitude";
	
	
	
	//Query for creating teamname table
    String CREATE_TABLE1 = "CREATE TABLE " + TABLE_NAME + "("
    		+ KEY_ID + " INTEGER NOT NULL , " + KEY_NAME + " TEXT , "
    		+ KEY_RANGE + " TEXT , "+ KEY_MESSAGE + " TEXT , "+ KEY_ALARM + " TEXT , "+ KEY_NOTIFICATION + " TEXT , "+ 
    		KEY_LONGITUDE+" TEXT ,"+ KEY_LATITUDE + " TEXT "+")";
    
  

    
    private Context context;
	private SQLiteDatabase sqLiteDatabase;
	private SQLiteOpenHelper sqLiteOpenHelper;
    
	public class SqliteOpenHelper extends SQLiteOpenHelper
	{

		public SqliteOpenHelper(Context context)
		{
			super(context,DATABASE_NAME, null,DATABASE_VERSION);
			Log.d("SqliteOpenHelper","SqliteOpenHelper");
		}

		@Override
		public void onCreate(SQLiteDatabase db)
		{
			Log.d("onCreate","onCreate");
			db.execSQL(CREATE_TABLE1);
			
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) 
		{
			Log.d("onUpgrade","onUpgrade");
			db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
			
			onCreate(db);
		}
		
	}
	
	DBLogic(Context context)
	{
		Log.d("DBLogic constructor","DBLogic constructor");
		this.context=context;
	}
	
	public void open() throws SQLException
	{
		Log.d("open","open");
		sqLiteOpenHelper =new SqliteOpenHelper(context);
		sqLiteDatabase= sqLiteOpenHelper.getWritableDatabase();		
	}
	
	public void close()
	{
		Log.d("close","close");
		sqLiteOpenHelper.close();
	}
	
	public void deleteTable()
	{
		Log.e("Delete table is fired","Delete table is fired");
		sqLiteDatabase.execSQL("Delete from "+TABLE_NAME);
	}
	
	
	// Adding new data to MYTABLE1 table
    public void Add_Data_To_STORYTable(MyBean data)
    {
    	Log.e("Add_Data_To_STORYTable","Add_Data_To_STORYTable");
    	ContentValues values = new ContentValues();
		values.put(KEY_ID, data.getId()); 
		values.put(KEY_NAME, data.getName()); 
		values.put(KEY_RANGE, data.getRange());
		values.put(KEY_MESSAGE, data.getMessage()); 
		values.put(KEY_ALARM, data.getAlarm());
		values.put(KEY_NOTIFICATION, data.getNotification());
		values.put(KEY_LONGITUDE, data.getLongitude());
		values.put(KEY_LATITUDE, data.getLatitude());
		
		sqLiteDatabase.insert(TABLE_NAME, null, values);		
    }
    
    
    public ArrayList<MyBean> getDataFromTAble()
    {
    	ArrayList<MyBean> al=new ArrayList<MyBean>();
    	
    	
    	Cursor cursor = sqLiteDatabase.query(TABLE_NAME, new String[] { KEY_ID,KEY_NAME,
    	  		KEY_RANGE,KEY_MESSAGE,KEY_ALARM,KEY_NOTIFICATION,KEY_LONGITUDE,KEY_LATITUDE},null,null, null, null, null, null);
    	
    	if (cursor != null)
      	    cursor.moveToFirst();
        	
        	for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
        	{
      			al.add(new MyBean(cursor.getString(0),cursor.getString(1),      				
      					cursor.getString(2),cursor.getString(3),cursor.getString(4),cursor.getString(5),cursor.getString(6), cursor.getString(7)));
        	}
        	cursor.close();
    	
    	
    	return al;
    }
   
    public Cursor countNumberOfRows(String[] str,String table_name)
    {
    	return sqLiteDatabase.query(table_name,str,null,null,null,null,null);
    }
    
       
    /*public Cursor getcount1(String lat, String lngs) {
		// TODO Auto-generated method stub
		return sqLiteDatabase.query(TABLE_NAME,new String[] {KEY_ID}, KEY5_LAT + " =? " + " AND " + KEY5_LONG + " =? " , new String[] {lat,lngs} , null,null,null,null);
		
	}*/
    

}

