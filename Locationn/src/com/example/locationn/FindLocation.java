package com.example.locationn;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

public class FindLocation implements LocationListener {
	
	protected LocationManager locationmanager;
	protected LocationListener locationlistener;
	protected Context context;
	
	public FindLocation(Context context )
	{
		this.context=context;
		locationmanager = (LocationManager)  context.getSystemService(Context.LOCATION_SERVICE);
		locationmanager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
		
	
	}
	
	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		location.getLatitude();
		location.getLongitude();
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}

}
