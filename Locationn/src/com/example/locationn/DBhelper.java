package com.example.locationn;


import java.util.ArrayList;



import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBhelper extends SQLiteOpenHelper{

		// Logcat tag
		private static final String LOG = "DatabaseHelper";

		// Database Version
		private static final int DATABASE_VERSION = 1;

		// Database Name
		private static final String DATABASE_NAME = "contactsManager";

		// Table Names
		private static final String TABLE_LOCATION_TABLE = "locationTable";
		private static final String TABLE_LOCATION_TABLE_LIST = "locationTableList";
		
		//Common Names
		private static final String KEY_ID = "id";
		private static final String KEY_NAMEHEADING= "name_heading";
		private static final String KEY_MESSAGE="message";
		private static final String KEY_RANGE="range";
		private static final String KEY_LATITUDE="latitude";
		private static final String KEY_LONGITUDE="longitude";
		private static final String KEY_TIME = "time";
		private static final String KEY_ADDRESS = "address";
		
				
		
		//Mode Table create statement
		private static final String CREATE_TABLE_LOCATION_DATA = "CREATE TABLE " + TABLE_LOCATION_TABLE
				+ "(" + KEY_ID + " INTEGER PRIMARY KEY," 
				+ KEY_NAMEHEADING + " TEXT, " 
				+ KEY_MESSAGE + " TEXT, "
				+ KEY_RANGE + " INTEGER, "
				+ KEY_LATITUDE + " TEXT, "
				+ KEY_LONGITUDE + " TEXT, "
				+ KEY_TIME + " INTEGER, "
				+ KEY_ADDRESS + " TEXT "
				+")";
		
		
		private static final String CREATE_TABLE_LOCATION_DATA_LIST = "CREATE TABLE " + TABLE_LOCATION_TABLE_LIST
				+ "(" + KEY_ID + " INTEGER PRIMARY KEY," 
				+ KEY_NAMEHEADING + " TEXT, " 
				+ KEY_MESSAGE + " TEXT, "
				+ KEY_RANGE + " INTEGER, "
				+ KEY_LATITUDE + " TEXT, "
				+ KEY_LONGITUDE + " TEXT "
				+")";
		
		
		public DBhelper(Context context)
		{
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}


		@Override
		public void onCreate(SQLiteDatabase db) {
			
			db.execSQL(CREATE_TABLE_LOCATION_DATA);
			db.execSQL(CREATE_TABLE_LOCATION_DATA_LIST);
		}


		@Override
		public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
			arg0.execSQL("DROP TABLE IF EXISTS " + TABLE_LOCATION_TABLE);
			arg0.execSQL("DROP TABLE IF EXISTS " + TABLE_LOCATION_TABLE_LIST);
			
			
			onCreate(arg0);
		}
		
		

		public void closeDB() {
			SQLiteDatabase db = this.getReadableDatabase();
			if (db != null && db.isOpen())
				db.close();
		}

		
		
		public long createLocationTable (LocationData locationdata) {
			SQLiteDatabase db = this.getWritableDatabase();

			ContentValues values = new ContentValues();
			values.put(KEY_NAMEHEADING, locationdata.getNameHeading());
			values.put(KEY_LATITUDE,locationdata.getLatitude());
			values.put(KEY_LONGITUDE, locationdata.getLongitude());
			values.put(KEY_MESSAGE, locationdata.getMessage());
			values.put(KEY_RANGE, locationdata.getRange());
			values.put(KEY_ADDRESS, locationdata.getAddress());
			Log.e("Entering time = "+ locationdata.getTime(), " in the database");
			values.put(KEY_TIME, locationdata.getTime());
			
			// insert row
			long location_id = db.insert(TABLE_LOCATION_TABLE, null, values);
			Log.e("Mode table id", location_id+"");				
			
			return location_id;
		}
		
		public void updateLocationTable(LocationData locationdata)
		{
			
				SQLiteDatabase db = this.getWritableDatabase();
				ContentValues values = new ContentValues();
				
				
					
				values.put(KEY_NAMEHEADING, locationdata.getNameHeading());
				values.put(KEY_LATITUDE,locationdata.getLatitude());
				values.put(KEY_LONGITUDE, locationdata.getLongitude());
				values.put(KEY_MESSAGE, locationdata.getMessage());
				values.put(KEY_RANGE, locationdata.getRange());
				values.put(KEY_TIME, locationdata.getTime());
				values.put(KEY_ADDRESS, locationdata.getAddress());
				db.update(TABLE_LOCATION_TABLE, values, KEY_NAMEHEADING + " = ?   ",
						new String[] {locationdata.getNameHeading() });
				
				
				
				
			
		}
		
		public void deleteLocationTableData (long id)
		{
			SQLiteDatabase db = this.getWritableDatabase();
			db.delete(TABLE_LOCATION_TABLE, KEY_ID + " = ?", new String[]{String.valueOf(id)});
		}
		
		
		public long createLocationTableList (LocationData locationdata) {
			SQLiteDatabase db = this.getWritableDatabase();

			ContentValues values = new ContentValues();
			values.put(KEY_NAMEHEADING, locationdata.getNameHeading());
			values.put(KEY_LATITUDE,locationdata.getLatitude());
			values.put(KEY_LONGITUDE, locationdata.getLongitude());
			values.put(KEY_MESSAGE, locationdata.getMessage());
			values.put(KEY_RANGE, locationdata.getRange());
			
			// insert row
			long location_id = db.insert(TABLE_LOCATION_TABLE_LIST, null, values);
			Log.e("Mode table id", location_id+"");				
			
			return location_id;
		}
		
		
		public ArrayList<LocationData> getAllLocationData()
		{
			ArrayList<LocationData> locationdata = new ArrayList<LocationData>();
			String selectQuery = "SELECT  * FROM " + TABLE_LOCATION_TABLE;

			Log.e(LOG, selectQuery);

			SQLiteDatabase db = this.getReadableDatabase();
			Cursor c = db.rawQuery(selectQuery, null);
			
			if (c.moveToFirst()) {
				do {
					LocationData td = new LocationData();
					td.setId(c.getInt((c.getColumnIndex(KEY_ID))));
					td.setLatitude(Double.parseDouble(c.getString(c.getColumnIndex(KEY_LATITUDE))));
					td.setLongitude(Double.parseDouble(c.getString(c.getColumnIndex(KEY_LONGITUDE))));
					td.setMessage(c.getString(c.getColumnIndex(KEY_MESSAGE)));
					td.setNameHeading(c.getString(c.getColumnIndex(KEY_NAMEHEADING)));
					td.setRange(c.getString(c.getColumnIndex(KEY_RANGE)));
					td.setTime(c.getLong(c.getColumnIndex(KEY_TIME)));
					td.setAddress(c.getString(c.getColumnIndex(KEY_ADDRESS)));
					locationdata.add(td);
				} while (c.moveToNext());
				
				return locationdata;
			}

				else
					return null;
		}
		
		//----------------------------------------------------------
		
		public ArrayList<LocationData> getAllLocationDataList()
		{
			ArrayList<LocationData> locationdata = new ArrayList<LocationData>();
			String selectQuery = "SELECT  * FROM " + TABLE_LOCATION_TABLE_LIST;

			Log.e(LOG, selectQuery);

			SQLiteDatabase db = this.getReadableDatabase();
			Cursor c = db.rawQuery(selectQuery, null);
			
			if (c.moveToFirst()) {
				do {
					LocationData td = new LocationData();
					td.setId(c.getInt((c.getColumnIndex(KEY_ID))));
					td.setLatitude(Double.parseDouble(c.getString(c.getColumnIndex(KEY_LATITUDE))));
					td.setLongitude(Double.parseDouble(c.getString(c.getColumnIndex(KEY_LONGITUDE))));
					td.setMessage(c.getString(c.getColumnIndex(KEY_MESSAGE)));
					td.setNameHeading(c.getString(c.getColumnIndex(KEY_NAMEHEADING)));
					td.setRange(c.getString(c.getColumnIndex(KEY_RANGE)));
					locationdata.add(td);
				} while (c.moveToNext());
				
				return locationdata;
			}

				else
					return null;
		}
		
		
		//---------------------------------------------------------------------------
		
		public void deleteLocationData(long location_id)
		{
			SQLiteDatabase db = this.getWritableDatabase();
			/*ContentValues values = new ContentValues();*/
			db.delete(TABLE_LOCATION_TABLE,KEY_ID + " = ? " , new String[]{location_id+"" });
		}
		
		//--------------------------------------------------------
		public void deleteLocationDataList(long location_id)
		{
			SQLiteDatabase db = this.getWritableDatabase();
			/*ContentValues values = new ContentValues();*/
			db.delete(TABLE_LOCATION_TABLE_LIST,KEY_ID + " = ? " , new String[]{location_id+"" });
		}
		
		//--------------------------------------------------------
		
}