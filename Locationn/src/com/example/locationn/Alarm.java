package com.example.locationn;

import java.io.IOException;

import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class Alarm extends Activity{
	
	private MediaPlayer mp;
	private PowerManager.WakeLock wlock;

	TextView t1,t2;
@SuppressWarnings("deprecation")
@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);

	this.requestWindowFeature(Window.FEATURE_NO_TITLE);
	setContentView(R.layout.alarm);
	
	t1=(TextView)findViewById(R.id.ReminderAlarm);
	t2=(TextView)findViewById(R.id.timee);
	t1.setText(First.message5);
	t2.setText(First.hour5+" : " + First.minute5);
	
	PowerManager powman = (PowerManager)getSystemService(Context.POWER_SERVICE);
	wlock= powman.newWakeLock(PowerManager.FULL_WAKE_LOCK, "My Wake Log");
	wlock.acquire(); // for turning off the title at the top of the page
	this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | 
			WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON, WindowManager.LayoutParams.FLAG_FULLSCREEN |
			WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | 
			WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
	
	
	Button stopAlarm= (Button) findViewById(R.id.stopalarm);
	stopAlarm.setOnClickListener( new Button.OnClickListener()
	{

		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			mp.stop();
			Dataaaa ddddd= new Dataaaa(Alarm.this);
			ddddd.open();
			ddddd.deleteEntryT2(First.message5, First.day5, First.month5, First.hour5 ,First.minute5);
			ddddd.close();
			finish();
		}
		
	});
	playSound(this, getAlarmUri());
}

private void playSound(Context context, Uri alert)
{
	mp = new MediaPlayer();
	
	try
	{
		mp.setDataSource(context,alert);
		
		final AudioManager audioManager= (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
		
		// if the phone is not mute, set the alarm else don't
		if(audioManager.getStreamVolume(AudioManager.STREAM_ALARM)!=0)
		{
			mp.setAudioStreamType(AudioManager.STREAM_ALARM);
			mp.prepare();
			mp.start();
		}
		
	}
	 catch(IOException e)
	 {
		 Log.i("Alarm Receiver","No Audio Files found");
	 }
	
}

private Uri getAlarmUri()
{
	Uri alert= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
	
	if(alert==null)
	{
		alert= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		if(alert== null)
		{
			alert= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
		}
	}
	return alert;
}

protected void stop()
{
	super.onStop();
	wlock.release(); 
}

}
