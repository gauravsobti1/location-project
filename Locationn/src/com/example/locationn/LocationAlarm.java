package com.example.locationn;

import java.util.HashMap;

import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

public class LocationAlarm extends Fragment{
	
	private TextView goToLocation;
	Location loc=null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View layoutView = inflater.inflate(R.layout.go_to_location_alarm, container,false); 
		
		goToLocation = (TextView ) layoutView.findViewById(R.id.SetLocationText);
		
		
		return layoutView;
	}

	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		goToLocation.setOnClickListener(new OnClickListener() {
			
			

			@Override
			public void onClick(View v) {
				
				int i =0;
				/*do
				{ i++;
				 Log.e("i =", i+"");
					returnedLocation=tryNgetLocation();
					if(i==100)
					{
						Toast.makeText(getActivity().getApplicationContext(), "Unable To get Location! Please Try Again! ", Toast.LENGTH_SHORT).show();
						break;
					}
						
						
					
				}while(returnedLocation==null);*/
				
				loc=null;
				final LocationManager location_manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
				
				
				
				
				       final LocationListener myListener  = new LocationListener() {
						
						@Override
						public void onStatusChanged(String provider, int status, Bundle extras) {
							// TODO Auto-generated method stub
							
						}
						
						@Override
						public void onProviderEnabled(String provider) {
							// TODO Auto-generated method stub
							
						}
						
						@Override
						public void onProviderDisabled(String provider) {
							// TODO Auto-generated method stub
							
						}
						
						@Override
						public void onLocationChanged(Location location) {
							Log.e("Inside Location Changed" , "Inside Location Changed");
							Toast.makeText(getActivity(), "Inside LocationChannged", Toast.LENGTH_LONG).show();
							loc=location;
							
							
							
						}
					};
					
					/*Criteria criteria = new Criteria();
			         criteria.setAccuracy(Criteria.ACCURACY_FINE);
			         criteria.setAltitudeRequired(false);
			         criteria.setBearingRequired(false);
			         criteria.setPowerRequirement(Criteria.POWER_LOW);
			         
			         String provider = location_manager.getBestProvider(criteria, true);*/
					
						/*location_manager.requestLocationUpdates(
								LocationManager.GPS_PROVIDER, 0, 0, myListener);*/
					
						Looper myLooper = Looper.myLooper();
						/*Criteria criteria = new Criteria();
						criteria.setAccuracy(Criteria.ACCURACY_COARSE);
						criteria.setSpeedAccuracy(Criteria.ACCURACY_LOW);
						String provider = location_manager.getBestProvider(criteria, true);
				location_manager.requestSingleUpdate(provider, myListener, myLooper);*/
						/*if(location_manager.isProviderEnabled(LocationManager.GPS_PROVIDER))*/
				location_manager.requestSingleUpdate(LocationManager.NETWORK_PROVIDER, myListener, myLooper);
				final ProgressDialog progress = ProgressDialog.show(getActivity(), "Fetching Location", "Please Wait... ");
				/*location_manager.requestSingleUpdate(LocationManager.NETWORK_PROVIDER, myListener, myLooper);*/
				Runnable newrunnable = new Runnable() {
					
					@Override
					public void run() {
						
						 location_manager.removeUpdates(myListener);
			             Log.e("Inside Handler","Inside Handler");
			             if(loc!=null)
							{Toast.makeText(getActivity().getApplicationContext(), "Latitude = " + loc.getLatitude()
									+ " Longitude = " + loc.getLongitude()
									, Toast.LENGTH_SHORT).show();
							Log.e( "Latitude = " + loc.getLatitude(), " Longitude = " + loc.getLongitude());
							goToLocation.setText("Latitude = " + loc.getLatitude()+ " Longitude = " + loc.getLongitude() + "Accuracy = " + loc.getAccuracy());
							
							SharedPreferences sharedPref = getActivity().getSharedPreferences("My Preference", Context.MODE_PRIVATE);
							SharedPreferences.Editor editor = sharedPref.edit();
							  editor.putString("latitude",String.valueOf(loc.getLatitude()));
							  editor.putString("longitude",String.valueOf(loc.getLongitude()));
							  
							  editor.commit();
							
							((ProjectActivity)getActivity()).fragment = new SetLocationAlarm();
							((ProjectActivity)getActivity()).addToBackStackFragment(getActivity().getResources().getString(R.string.SetLocationFragment));
							
							  /*startActivity(new Intent(getActivity(), SetLocationAlarm.class));*/
							  progress.dismiss();
							}
			             else
			             {
			            	 Toast.makeText(getActivity(), "Sorry Can't get Location", Toast.LENGTH_LONG).show();
			            	/* ((ProjectActivity)getActivity()).fragment = new SetLocationAlarm();
								((ProjectActivity)getActivity()).addToBackStackFragment(getActivity().getString(R.string.SetLocationFragment));;*/
			            	 progress.dismiss();
			             }
						
					}
				};
				Handler myHandler = new Handler(myLooper);
			    myHandler.postDelayed(newrunnable, 5000);
				
				
				
			}
		});
		
		
	}
	
	void dowork()
	{
		
	}
	
	public Location tryNgetLocation()
	{
		loc=null;
		final LocationManager location_manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
		
		
		
		
		       final LocationListener myListener  = new LocationListener() {
				
				@Override
				public void onStatusChanged(String provider, int status, Bundle extras) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onProviderEnabled(String provider) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onProviderDisabled(String provider) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onLocationChanged(Location location) {
					Log.e("Inside Location Changed" , "Inside Location Changed");
					Toast.makeText(getActivity(), "Inside LocationChannged", Toast.LENGTH_LONG).show();
					loc=location;
					
					
				}
			};
			
			/*Criteria criteria = new Criteria();
	         criteria.setAccuracy(Criteria.ACCURACY_FINE);
	         criteria.setAltitudeRequired(false);
	         criteria.setBearingRequired(false);
	         criteria.setPowerRequirement(Criteria.POWER_LOW);
	         
	         String provider = location_manager.getBestProvider(criteria, true);*/
			
				/*location_manager.requestLocationUpdates(
						LocationManager.GPS_PROVIDER, 0, 0, myListener);*/
			
				Looper myLooper = Looper.myLooper();
				
		location_manager.requestSingleUpdate(LocationManager.GPS_PROVIDER, myListener, myLooper);
		location_manager.requestSingleUpdate(LocationManager.NETWORK_PROVIDER, myListener, myLooper);
		final Handler myHandler = new Handler(myLooper);
	    myHandler.postDelayed(new Runnable() {
	         public void run() {
	             location_manager.removeUpdates(myListener);
	             Log.e("Inside Handler","Inside Handler");
	         }
	    }, 10000);
		
				/*location_manager.requestLocationUpdates(
						LocationManager.NETWORK_PROVIDER, 0, 0, myListener);*/
		
				
				/*if(loc.getAccuracy() >5)
				{
					
				}
				else
					loc=null;*/
			
		/*Location loc = location_manager.getLastKnownLocation(LocationManager.GPS_PROVIDER);*/
		
		
		/*location_manager.removeUpdates(myListener);*/
				
		if(loc !=null )
		{
			Log.e("Inside loc!=null","Inside loc!=null");
			/*if(loc.getAccuracy() > 5)*/
		return loc;
			
				
		}
		
		else
			{
			Log.e("Inside loc==null","Inside loc==null");
			return null;
			}
				
		
	}
	
	
	
	
	
	public class MyTask extends AsyncTask<Void, Void, Void>
	{
		
		ProgressDialog progress;
		@Override
		protected void onPreExecute() {
			
			progress=ProgressDialog.show(getActivity(), "Getting Updates", " Please Wait");
			progress.setCancelable(true);
			super.onPreExecute();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			
			while(true)
			{if (isCancelled())
				{break;
				
				}
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progress.dismiss();
		}
	}
	
	
	
}
