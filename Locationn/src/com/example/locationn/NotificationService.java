package com.example.locationn;

import java.util.ArrayList;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class NotificationService extends Service {

	Dataaaa d;
	NotificationManager nm;
	PendingIntent pi;
	Intent intent;
	ArrayList <String> latitude,longitude,Range,address;
	ArrayList<String> name, message;
	LocationManager locman;
	String [] ran,lat,lon;
	Double[] lat1,long1;
	ArrayList<Double> lat2,long2;
	Location  compLoc;
	int size;
	Notification n;
	
	int i=0;
	
	public static String my_msg,my_range;
	public static String my_name;
	public static String my_add;


	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onCreate() {
		Toast.makeText(this, "Service Created", Toast.LENGTH_LONG).show();  
		Log.e("Service created ","Service created");
		
		//d=new Dataaaa (NotificationService.this);
		try
		{
			d=new Dataaaa(getApplicationContext());
			d.open();
			size=d.getkeylatitude().size();
			Log.e("Size",size+"");
			latitude=d.getkeylatitude();
			longitude=d.getkeylongitude();
			Log.e("Latitude= ",latitude.toString());
			Log.e("Longitude= ",longitude.toString());
			name= d.getName();
			message= d.getMessage();
			address= d.getkeyAddress();
			Range=d.getRange();
			my_add=address.get(0);
			my_name=name.get(0);
			my_msg=message.get(0);
			my_range=Range.get(0);
			//longitude=d.getkeylongitude();
			

			Log.e("Range= ", Range.toString());
			d.close();
		}
		catch(Exception e)
		{
			stopSelf();
			
		}




		//Log.e("Range=",Range.toString());



		compLoc= new Location(LocationManager.NETWORK_PROVIDER);
/*
	//	Log.e("complac size",""+compLoc.length);
		Log.e("latitude size",""+latitude.size());
		Log.e("longitude size",""+longitude.size());

		for(int i=0;i<size;i++)
		{
			try 
			{
				compLoc.setLatitude(Double.parseDouble(latitude.get(0)));
				compLoc.setLongitude(Double.parseDouble(longitude.get(0)));
			} 
			catch (Exception e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		*/
		
		/*Log.e("longitude size",""+compLoc[0].getLatitude());*/

		/*for(int i=0;i<size;i++)
		{

			try 
			{
				compLoc[i].setLatitude(Double.parseDouble(latitude.get(i)));
				Log.e("Double latitude",(Double.parseDouble(latitude.get(i)))+"");
				compLoc[i].setLongitude(Double.parseDouble(longitude.get(i)));
				Log.e("Double longitude",(Double.parseDouble(longitude.get(i)))+"");
			}
			catch (NumberFormatException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}*/

	
		nm= (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		intent= new Intent(NotificationService.this,Notify.class);

		pi= PendingIntent.getActivity(NotificationService.this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT|PendingIntent.FLAG_ONE_SHOT);

	}

	@Override
	public void onStart(Intent intent, int startId) {
		




		//Log.e("Range=",Range.toString());



		compLoc= new Location(LocationManager.NETWORK_PROVIDER);

		/*Log.e("complac size",""+compLoc.length);
		Log.e("latitude size",""+latitude.size());
		Log.e("longitude size",""+longitude.size());
*/
		/*for(int i=0;i<size;i++)
		{*/
		
		for(i=0;i<latitude.size();i++)
			{
			try 
			
			{
				compLoc.setLatitude(Double.parseDouble(latitude.get(i)));
				compLoc.setLongitude(Double.parseDouble(longitude.get(i)));
			} 
			catch (Exception e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	

		Log.e("Service has started","Service has started");
		locman= (LocationManager) NotificationService.this.getSystemService(Context.LOCATION_SERVICE);
		LocationListener listen = new LocationUpdate();
		locman.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10*60*1000, 500 , listen);
		
	}
	}

	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		Toast.makeText(NotificationService.this, "Service destroyed", Toast.LENGTH_LONG).show();
		super.onDestroy();
	}



	public class LocationUpdate implements LocationListener{





		@SuppressWarnings("deprecation")
		@Override
		public void onLocationChanged(Location location) {
			// TODO Auto-generated method stub
			
			Log.e("Inside onLocationChanged","Inside onLocationChanged");


			//		if((location.getLatitude()) && (location.getLongitude())
			//for(int i=0;i<size;i++)
			//{
		//	Log.e("Range= ", Range.get(0));
			Log.e("distance to= ", (location.distanceTo(compLoc))+"");
			Log.e("Latitude and Longitude inside LocationChange=",location.getLatitude()+"  "+location.getLongitude());
			if(location.distanceTo(compLoc)<= (Integer.parseInt(Range.get(i))*1000))
					{	
						Log.e("Inside compare block","Inside compare block");	
							//n= new Notification(R.drawable.ic_launcher,message.get(0),System.currentTimeMillis());
							//n.setLatestEventInfo(NotificationService.this, name.get(0), message.get(0), pi);
							//n.defaults=Notification.DEFAULT_ALL;
							
							Log.e("Before Notification","Before Notification");
							n= new Notification.Builder(NotificationService.this).setContentTitle(name.get(i)).setContentText(message.get(i)+"\n"+address.get(i))
									.setSmallIcon(R.drawable.ic_launcher).setContentIntent(pi).setAutoCancel(true).build();				


							nm.notify(255,n);
							Log.e("After Notify","After Notify");
							
							
					}
		//	}

		}

		@Override
		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub

		}

	}



}
