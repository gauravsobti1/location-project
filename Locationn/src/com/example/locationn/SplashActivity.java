package com.example.locationn;

import com.example.location.update.AppController;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.Window;

public class SplashActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.splash_activity);
		
		
		SharedPreferences sharedPref = this.getSharedPreferences("My Preference", Context.MODE_PRIVATE);
		if(sharedPref.getInt("alarm_started", 0)==0)
		{
			Log.e("Starting alarm inside activity","Starting alarm inside activity");
		AlarmManager alarmMgr = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
	      
	    //  PendingIntent pendingIntent =
	       //        PendingIntent.getBroadcast(this, 0, new Intent(this, DealAlarmReceiver.class), 0);

	      // use inexact repeating which is easier on battery (system can phase events and not wake at exact times)
	      /*alarmMgr.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + 30000,
	               AlarmManager.INTERVAL_FIFTEEN_MINUTES, pendingIntent);*/
	      alarmMgr.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + 30000,
	               2*60*1000, AppController.getInstance().getPendingIntent());
	    //  alarmMgr.cancel(pendingIntent);
	     
	      SharedPreferences.Editor editor = sharedPref.edit();
		  editor.putInt("alarm_started",1);
		  
		  
		  editor.commit();
		
		}
		
		  

		
		new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... params) {
				try {
					Thread.sleep(4000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return null;
			}
			
			protected void onPostExecute(Void result) {
				startActivity(new Intent(SplashActivity.this,ProjectActivity.class));
				finish();
				
			}
			
		}.execute();
		
		Log.e("Distance between two locations = " , getDistanceFromLatLonInKm(28.661528324010078, 77.13532057546036, 28.6275183, 77.1111214	) +"" );
		
	}
	
	
	
	/*
	private double distance_bw_locations(double lat2, double lon2, double lat1, double lon1) {
	      double theta = lon1 - lon2;
	      double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
	      
	      
	      dist = Math.acos(dist);
	      dist = rad2deg(dist);
	      dist = dist * 60 * 1.1515;
	      
	      Log.e("distance between locations = ", dist+"");
	       return (dist);
	    }

	   private double deg2rad(double deg) {
	      return (deg * Math.PI / 180.0);
	    }
	   private double rad2deg(double rad) {
	      return (rad * 180.0 / Math.PI);
	    }
	*/
	
	
	public double getDistanceFromLatLonInKm(double lat1,double lon1,double lat2,double lon2) {
		  double R = 6371; // Radius of the earth in km
		  double dLat = deg2rad(lat2-lat1);  // deg2rad below
		  double dLon = deg2rad(lon2-lon1); 
		  double a = 
		    Math.sin(dLat/2) * Math.sin(dLat/2) +
		    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
		    Math.sin(dLon/2) * Math.sin(dLon/2)
		    ; 
		  double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
		  double d = R * c; // Distance in km
		  return d;
		}

		double deg2rad(double deg) {
		  return deg * (Math.PI/180);
		}
	
	
}
