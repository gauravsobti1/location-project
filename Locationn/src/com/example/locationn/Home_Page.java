package com.example.locationn;

import java.util.ArrayList;

import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class Home_Page extends Fragment {
	
	private ListView home_page_list;
	private TextView noAlarmSet;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		View layoutView = inflater.inflate(R.layout.home_page, container,false);
		home_page_list= (ListView) layoutView.findViewById(R.id.listView1);
		noAlarmSet = (TextView) layoutView.findViewById(R.id.textView1);
		return layoutView;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		/*LocationData locationdata = new LocationData();
		locationdata.setLatitude(28.66154364413619);
		locationdata.setLongitude(77.13500999161681);
		locationdata.setRange(String.valueOf(2000));
		locationdata.setNameHeading("Sudershan Park");
		locationdata.setMessage("You are at Home");
		
		
		DBhelper db = new DBhelper(getActivity());
		db.createLocationTable(locationdata);
		db.closeDB();*/
		
		DBhelper db = new DBhelper(getActivity());
		ArrayList<LocationData> LocData = db.getAllLocationData();
		db.closeDB();
		if(LocData!=null)
		{
			MyAdapter adapter = new MyAdapter(getActivity(), R.layout.homepage_listview, LocData);
		home_page_list.setAdapter(adapter);
		}
		else
			{
			noAlarmSet.setVisibility(View.VISIBLE);
			noAlarmSet.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					
					
				}
			});
			}
	}

	public class MyAdapter extends ArrayAdapter<LocationData>
	{
	
		
		private Context context;
		

		public MyAdapter(Context context, int resource, ArrayList<LocationData> locData) {
			super(context, resource,locData);
			this.context = context;
			
		}
		
		@Override
		public int getCount() {
			
			return super.getCount();
			
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			final LocationData data= getItem(position);
			View row = convertView;
			if(row==null)
			{
				row = inflater.inflate(R.layout.homepage_listview,parent,false );
			}
			
			TextView name = (TextView) row.findViewById(R.id.textView1);
			TextView range = (TextView) row.findViewById(R.id.textView2);
			ImageView delete_data = (ImageView) row.findViewById(R.id.imageView1);
			
			name.setText(data.getNameHeading());
			range.setText(data.getRange()+ " km");
			delete_data.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					DBhelper db = new DBhelper(context);
					
					db.deleteLocationTableData(data.getId());
					
					db.closeDB();
					((ProjectActivity)context).fragment = new Home_Page();
					((ProjectActivity)context).addToBackStackFragment(context.getResources().getString(R.string.HomePageFragment));
				}
			});
			
			return row;
		}
		
		
	}
	
	
}
