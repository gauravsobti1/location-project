package com.example.locationn;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Build;

public class Notify extends Activity {
String name,message,address,range;
TextView t1,t2,t3;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_notify);
		//PendingIntent pii= PendingIntent.getBroadcast(this, id, i, 
          //      PendingIntent.FLAG_UPDATE_CURRENT);
		Intent iiin= getIntent();
		stopService(iiin);
		Toast.makeText(Notify.this, "Inside notify", Toast.LENGTH_LONG).show();
		name=NotificationService.my_name;
		message=NotificationService.my_msg;
		address=NotificationService.my_add;
		range=NotificationService.my_range;
		
		t1= (TextView) findViewById(R.id.textView1noti);
		t2=(TextView) findViewById(R.id.textView2noti);
		t3=(TextView) findViewById(R.id.textView3noti);
		
		t1.setText(NotificationService.my_name);
		t2.setText(NotificationService.my_msg);
		t3.setText(NotificationService.my_add);
		
		
		Dataaaa ddd= new Dataaaa(Notify.this);
		ddd.open();
		ddd.deleteEntryT1(name, message, address,range);
		Log.e("Database opened","Database opened");
		try
		{
		Log.e("Values in database", ddd.getName().toString()+"\n " + ddd.getkeyAddress().toString());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		ddd.close();
		Intent in= new Intent(Notify.this, NotificationService.class);
		stopService(in);
		startService(in);
			
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.notify, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	
}
