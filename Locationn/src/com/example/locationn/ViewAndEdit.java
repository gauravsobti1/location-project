package com.example.locationn;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


public class ViewAndEdit extends Activity{

ListView lw;
ArrayList<String> reminder,datee,time,month,hour,minute;
int i=0;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.viewnedit);
		
		
		lw=(ListView) findViewById(R.id.listView1);
		Dataaaa data = new Dataaaa(ViewAndEdit.this);
		data.open();
		
		
		reminder=(data.getReminder());
		datee=(data.getDate());
		time=(data.getTime());
		month=data.getMonth();
		hour=data.getHour();
		minute=data.getminute();
		
		data.close();
		String [] reminder2= new String[(reminder.size())];
		String [] date2= new String[(datee.size())];
		String [] time2= new String[(time.size())];
	reminder2=reminder.toArray(reminder2);
	date2=datee.toArray(date2);
	time2=time.toArray(time2);
	
		
			
//		for(int i=0;i<result.size();i++)
//		{
			
				
			
//		}
		Log.e("Reminder",reminder.toString());
		Log.e("Date",datee.toString());
		Log.e("Time",time.toString());
		
		myAdapter ma= new myAdapter(ViewAndEdit.this, reminder2, date2, time2);
		lw.setAdapter(ma);
	/*	if(rem==null)
		{
			Toast.makeText(ViewAndEdit.this, "Error", Toast.LENGTH_LONG).show();
		}
			ArrayAdapter<String> adapter = new ArrayAdapter<>(ViewAndEdit.this,R.layout.listview,R.id.reminder ,rem );
			lw.setAdapter(adapter);
	*/	
	}
	
	public class myAdapter extends ArrayAdapter<String>
	{
Context context;
String [] reminder1,date1,time1;



		public myAdapter(Context c, String[] remind,String[] dat,String [] tim) {
			super(c,R.layout.listview, R.id.reminder,remind);
			
			this.context=c;
			this.reminder1=remind;
			this.date1=dat;
			this.time1=tim;
		}
		
		@Override
				public View getView(int position, View convertView, ViewGroup parent) {
					// TODO Auto-generated method stub
		 final int positi= position+1;
		 final int positio=position;
					LayoutInflater inflater= (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					View row= inflater.inflate(R.layout.listview,parent,false);
					TextView message=(TextView)row.findViewById(R.id.reminder);
					TextView date= (TextView)row.findViewById(R.id.date);
					TextView time= (TextView)row.findViewById(R.id.time);
					TextView edit=(TextView)row.findViewById(R.id.edit);
					TextView delete=(TextView)row.findViewById(R.id.delete);
					message.setText(reminder1[position]);
					date.setText(date1[position]);
					time.setText(time1[position]);
					
					edit.setOnClickListener(new ListView.OnClickListener()
					{
							
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							Intent newintent = new Intent(ViewAndEdit.this, EditAlarm.class);
							newintent.putExtra("Row", positi);
							startActivity(newintent);
							finish();
						}
						
					});
					
					delete.setOnClickListener(new ListView.OnClickListener()
					{
							
						@Override
						public void onClick(View v) {
							
							
							// TODO Auto-generated method stub
							Dataaaa dd= new Dataaaa(ViewAndEdit.this);
							
							dd.open();
							dd.deleteEntryT2(reminder.get(0),datee.get(0), month.get(0), hour.get(0), minute.get(0));
							
							dd.close();
							Log.e("position1 = ",positio+"");
							Log.e("position = ",positi+"");
							Toast.makeText(ViewAndEdit.this, "Data from row has been deleted", Toast.LENGTH_LONG).show();
							
							Intent newintent = new Intent(ViewAndEdit.this, First.class);
							startActivity(newintent);
							finish();
						}
						
					});
					
					
					return row;		
		}
		
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		return super.onOptionsItemSelected(item);
	}
	@Override
	public void onOptionsMenuClosed(Menu menu) {
		// TODO Auto-generated method stub
		super.onOptionsMenuClosed(menu);
	}
}
