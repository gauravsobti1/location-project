package com.example.locationn;

import java.util.ArrayList;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

public class DealAlarmReceiver extends BroadcastReceiver{

	
	private Location loc;
	
	
	
	@Override
	public void onReceive(final Context context, Intent intent) {
		
		Log.e("Inside Deal Alarm Receiver", "onReceive");
		
		loc=null;
		
		final LocationManager location_manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		
		
		
		
		       final LocationListener myListener  = new LocationListener() {
				
				@Override
				public void onStatusChanged(String provider, int status, Bundle extras) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onProviderEnabled(String provider) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onProviderDisabled(String provider) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onLocationChanged(Location location) {
					Log.e("Inside Location Changed" , "Inside Location Changed");
					//Toast.makeText(context, "Inside LocationChannged", Toast.LENGTH_LONG).show();
					loc=location;
					
					
					
				}
			};
			
			/*Criteria criteria = new Criteria();
	         criteria.setAccuracy(Criteria.ACCURACY_FINE);
	         criteria.setAltitudeRequired(false);
	         criteria.setBearingRequired(false);
	         criteria.setPowerRequirement(Criteria.POWER_LOW);
	         
	         String provider = location_manager.getBestProvider(criteria, true);*/
			
				/*location_manager.requestLocationUpdates(
						LocationManager.GPS_PROVIDER, 0, 0, myListener);*/
			
				Looper myLooper = Looper.myLooper();
				/*Criteria criteria = new Criteria();
				criteria.setAccuracy(Criteria.ACCURACY_COARSE);
				criteria.setSpeedAccuracy(Criteria.ACCURACY_LOW);
				String provider = location_manager.getBestProvider(criteria, true);
		location_manager.requestSingleUpdate(provider, myListener, myLooper);*/
		location_manager.requestSingleUpdate(LocationManager.NETWORK_PROVIDER, myListener, myLooper);
		Runnable newrunnable = new Runnable() {
			
			@Override
			public void run() {
				
				 location_manager.removeUpdates(myListener);
	             Log.e("Inside Handler","Inside Handler");
	             if(loc!=null)
					{Toast.makeText(context, "Latitude = " + loc.getLatitude()
							+ " Longitude = " + loc.getLongitude()
							, Toast.LENGTH_SHORT).show();
					
						DBhelper db = new DBhelper(context);
						ArrayList<LocationData> locationArrayList = db.getAllLocationData();
						
						db.closeDB();
						
						
							if (locationArrayList!=null) {
								for (LocationData data : locationArrayList) {
									Log.v("Time in database = ", data.getTime()/1000/60/60+"" );
									Log.d("Current Time in milliseconds = ", System.currentTimeMillis()/1000/60/60+"");
									if(data.getTime()<= System.currentTimeMillis())
									{
										Log.e("Yes, you can look for updates", "Yes, you can look for updates");
									if ((getDistanceFromLatLonInKm(
											loc.getLatitude(),
											loc.getLongitude(),
											data.getLatitude(),
											data.getLongitude())) <= (Double
											.parseDouble(data.getRange()) + 0.5)) {
										Toast.makeText(
												context,
												"You have reached this location"
														+ data.getNameHeading(),
												Toast.LENGTH_SHORT).show();
										generateNotification(context, data);
										DBhelper db1 = new DBhelper(context);
										db1.deleteLocationData(data.getId());
										db1.closeDB();
										break;

									}
								  }
									else
									{
										Log.e("No you can not look for updates", "No you can not look for updates");
									}
								}
							}
						
					
					
					}
	             else
	             {
	            	 Toast.makeText(context, "Sorry can't get Location", Toast.LENGTH_LONG).show();
	             }
				
			}
		};
		Handler myHandler = new Handler(myLooper);
	    myHandler.postDelayed(newrunnable, 5000);
		
		
		
		
		
		
	}
	
		public void generateNotification(Context context, LocationData locationdata )
		{
			
			NotificationManager mManager = (NotificationManager) context
					.getSystemService(Context.NOTIFICATION_SERVICE);
			// build notification
			// the addAction re-use the same intent to keep the example short
			Intent intent1 = new Intent(context,
				DisplayNotification.class);
			intent1.putExtra("Name", locationdata.getNameHeading());
			intent1.putExtra("Message", locationdata.getMessage());
			intent1.putExtra("Addr", locationdata.getAddress());
			/*Bitmap bitmap = BitmapFactory.decodeByteArray(modeData.get(mode_id-1).getLeftDrawable() , 0, modeData.get(mode_id-1).getLeftDrawable().length);*/

			
			Notification notification = new Notification(R.drawable.app_iconn,
				"You have reached " + locationdata.getAddress(), System.currentTimeMillis());

			intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
					| Intent.FLAG_ACTIVITY_CLEAR_TOP);

			PendingIntent pendingNotificationIntent = PendingIntent.getActivity(
					context, 0, intent1,
					PendingIntent.FLAG_UPDATE_CURRENT);

			notification.flags |= Notification.FLAG_AUTO_CANCEL;

			
			
			notification.setLatestEventInfo(context, "You have reached ",
					locationdata.getAddress(), pendingNotificationIntent);

			mManager.notify(0, notification);
			
			Intent intent22 = new Intent(context,
					PlaySoundAlarm.class);
				intent22.putExtra("Name", locationdata.getNameHeading());
				intent22.putExtra("Message", locationdata.getMessage());
				intent22.putExtra("Addr", locationdata.getAddress());
			intent22.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
			/*intent22.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);*/
			context.startActivity(intent22);
			
			
			/*Uri notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
			r.play();
		*/
		
	}
	
	
	
	public Location tryNgetLocation(Context context)
	{
		LocationManager location_manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		
		
		
		
		       LocationListener myListener  = new LocationListener() {
				
				@Override
				public void onStatusChanged(String provider, int status, Bundle extras) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onProviderEnabled(String provider) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onProviderDisabled(String provider) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onLocationChanged(Location location) {
					// TODO Auto-generated method stub
					
				}
			};
			
			
		 location_manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0 ,myListener);  
		
			
		Location loc = location_manager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		
		location_manager.removeUpdates(myListener);
		if(loc !=null )
		{
		return loc;
		}
		
		else
			return null;
		
	}
	
	
	
	
	public double getDistanceFromLatLonInKm(double lat1,double lon1,double lat2,double lon2) {
		
		Log.d("lat1 = ", lat1+"");
		Log.d("lon1 = ", lon1+"");
		Log.d("lat2 = ", lat2+"");
		Log.d("lon2 = ", lon2+"");
		
		double R = 6371; // Radius of the earth in km
		  double dLat = deg2rad(lat2-lat1);  // deg2rad below
		  double dLon = deg2rad(lon2-lon1); 
		  double a = 
		    Math.sin(dLat/2) * Math.sin(dLat/2) +
		    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
		    Math.sin(dLon/2) * Math.sin(dLon/2)
		    ; 
		  double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
		  double d = R * c; // Distance in km
		  Log.e("Distance between two locations = ", d +"");
		  return d;
		}

		double deg2rad(double deg) {
		  return deg * (Math.PI/180);
		}
	
	
		

}
