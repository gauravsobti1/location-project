package com.example.locationn;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class StoreDataReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		
		Log.e("Inside Receive", "Store Data Receiver");
		LocationData locData = new LocationData();
		locData.setLatitude(intent.getDoubleExtra("double_latitude", 0.0));
		locData.setLongitude(intent.getDoubleExtra("double_longitude", 0.0));
		locData.setMessage(intent.getStringExtra("string_message"));
		locData.setNameHeading(intent.getStringExtra("string_name"));
		locData.setRange(intent.getStringExtra("string_range"));
		locData.setTime(intent.getIntExtra("int_time", 0));
		DBhelper db = new DBhelper(context);
		db.updateLocationTable(locData);;
		db.closeDB();
		
	}
	

}
