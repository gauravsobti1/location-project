package com.example.locationn;



import java.util.ArrayList;






import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class Locatio extends Activity{
	
	Intent nintent;
	EditText t,t1;
	TextView t3;
	CheckBox c1,c2;
	Button b;
	SeekBar s;
	String longitude;
	String latitude;
	String address,finalURL,Name,Range,Message;
	FindAddress obj;
	String url="https://api.foursquare.com/v2/venues/search?ll="; //writing ll to take value for latitude and longitude which have a common in between them
	String access="&oauth_token=SM2DT3QTFMGPVD4CH0KEAGO0IEMHEHW45HIMVVSAQEZUOXDJ&v=20140621";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.location);
		
		t=(EditText) findViewById(R.id.Message);
		t1=(EditText) findViewById(R.id.Name);
		b=(Button) findViewById(R.id.Ok);
		t.setText(null);
		s=(SeekBar) findViewById(R.id.seekBar1);
		t3=(TextView) findViewById(R.id.textView3);
		
		

		 longitude=(getIntent().getDoubleExtra("long", 0))+"";
		 latitude=(getIntent().getDoubleExtra("lat", 0))+"";
		 
		 Toast.makeText(Locatio.this, longitude+" "+latitude+" ", Toast.LENGTH_LONG).show();
		
		s.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				// TODO Auto-generated method stub
				t3.setText(s.getProgress()+"");
			}
		});
		
		
		b.setOnClickListener(new Button.OnClickListener()
		{

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				
				Message=  t.getText().toString();
				String s1=  t.getText().toString().trim();
				
				 Name= t1.getText().toString();
				 Range= t3.getText().toString();
				
				Log.e("Values","Message= "+Message+"Name = " +Name+ "Range= "+Range+ "Latitude= "+latitude+"Longitude= " +longitude);
				
				
				
				if(s1.equals(""))
				{
					//Toast.makeText(Location.this,s, Toast.LENGTH_LONG).show();
					
					Toast.makeText(Locatio.this,"No Message entered", Toast.LENGTH_LONG).show();
				}
				
				
				/*DBLogic databasee = new DBLogic(Locatio.this);
				databasee.open();
				MyBean bean = new MyBean(j,Name,Range,Message,Alarm1,Notification1,longitude,latitude);
				databasee.Add_Data_To_STORYTable(bean);*/
				
				finalURL= url + latitude + "," + longitude + access;
				myTask m= new myTask ();
				m.execute();
				
				
			}
			
		});
		
		
 		
		
		
	}

	
	class myTask extends AsyncTask<Void,String, Void>
	{
		@Override
		protected void onPreExecute() {
			obj = new FindAddress(finalURL);
		}
		
		
		@Override
		protected Void doInBackground(Void... params) {
			//obj = new FindAddress(finalURL);
		      obj.fetchJSON();
		      while(obj.parsingComplete);
		      
		    Log.e("do in background= ", obj.getresult() );  
			publishProgress(obj.getresult());
			
			return null;
		}
		
		
		@Override
		protected void onProgressUpdate(String... values) {
			// TODO Auto-generated method stub
			address= values[0];
		}
		
		
		@Override
		protected void onPostExecute(Void result) {
			
			Log.e("In post execute", address);
			Dataaaa da= new Dataaaa(Locatio.this);
			da.open();
			da.createEntry1(Name, Range, Message,latitude, longitude,address);
			
			da.close();
			
			
			
			Toast.makeText(Locatio.this, address, Toast.LENGTH_LONG).show();
			Toast.makeText(Locatio.this, "Data has been added", Toast.LENGTH_LONG).show();
			
			nintent = new Intent(Locatio.this,First.class);
			startActivity(nintent);
			finish();
			
			
		}
	}
	
}
