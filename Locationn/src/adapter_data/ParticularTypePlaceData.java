package adapter_data;

public class ParticularTypePlaceData {

	String place_id;
	String place_name;
	String image_url;
	String address;
	
	public ParticularTypePlaceData(String place_id, String place_name,
			String image_url, String address) {
		super();
		this.place_id = place_id;
		this.place_name = place_name;
		this.image_url = image_url;
		this.address = address;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getImage_url() {
		return image_url;
	}

	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}

	public ParticularTypePlaceData() {
		// TODO Auto-generated constructor stub
	}

	public String getPlace_id() {
		return place_id;
	}

	public void setPlace_id(String place_id) {
		this.place_id = place_id;
	}

	public String getPlace_name() {
		return place_name;
	}

	public void setPlace_name(String place_name) {
		this.place_name = place_name;
	}

	

	
	
	
	
}
